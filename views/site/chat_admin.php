<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\grid\GridView;
/* @var $this yii\web\View */

$this->title = "NIM : ".$datamahasiswa->nim;
// $sql = "
//     select 
//       count(1) as notifikasi
//     from percakapan 
//     where idmahasiswa = :idmahasiswa 
//     and idadminlayanan is not null 
//     and status_baca is null
//   ";
//   $datapercakapan = Yii::$app->db->createCommand($sql)
//     ->bindValues([
//       ':idmahasiswa'=> Yii::$app->user->identity->id_user
//      ])
//     ->queryAll();
    
$sql = "
  update 
    percakapan set status_baca = 1 
  where 
  idmahasiswa = :idmahasiswa 
  and idadminlayanan is null
  and status_baca is null
";
Yii::$app->db->createCommand($sql)
  ->bindValues([
    ':idmahasiswa'=> $_GET['Percakapan']['idmahasiswa']
   ])
  ->execute();
?>
<?php
// print_r($datamahasiswa);
?>
<div class="row">
  <div class="col-md-12">
    <!-- DIRECT CHAT -->
    <div class="box box-warning direct-chat direct-chat-warning">
      <div class="box-header with-border">
        <!-- <h3 class="box-title">Nim : <?=$datamahasiswa->nim?></h3> -->

        <div class="box-tools pull-right">
          <!-- <span data-toggle="tooltip" title="3 New Messages" class="badge bg-yellow">3</span> -->
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
          <!-- <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="Contacts"
                  data-widget="chat-pane-toggle">
            <i class="fa fa-comments"></i></button> -->
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
          </button>
        </div>
      </div>
      <!-- /.box-header -->
      <div class="box-body" style="height: 500px;">
        <!-- Conversations are loaded here -->
        <div class="direct-chat-messages" style="height: 500px;">
          <?php
          if(Yii::$app->user->identity->role=="Admin Layanan"){
            foreach ($dataProvider->getModels() as $key => $value) {
            ?>

            <?php if(empty($value->idadminlayanan)){?>
            <!-- Message to the left -->
              <div class="direct-chat-msg left">
                <div class="direct-chat-info clearfix">
                  <span class="direct-chat-name pull-left"><?=$value->mahasiswa->nama?></span>
                  <!-- <span class="direct-chat-timestamp pull-left"><?=$value->tanggal;?></span> -->
                </div>
                <!-- /.direct-chat-info -->
                <img class="direct-chat-img" src="<?=Yii::$app->request->baseUrl?>/images/no-person.jpg?>" alt="message user image">
                <!-- /.direct-chat-img -->
                <div class="direct-chat-text col-md-6 pull-left">
                  <div class="pull-left">
                    <div align="left">
                      <?php 
                        if(!empty($value->dokumen)){
                      ?>
                      <i class="fa fa-file-word-o" style="color: black; font-size: 15pt;"></i> <a href="../media/file_mahasiswa/<?=$value->dokumen?>" style="color: black; text-decoration: underline;"><?=$value->dokumen?></a>
                      <br>
                      <?php
                          }
                      ?>
                      <?=$value->isi;?>
                    </div>
                  </div>
                  
                  <br>
                  <span class="direct-chat-timestamp pull-right" style="color: white; font-size: 8pt;"><?=$value->tanggal;?></span>
                </div>
                <!-- /.direct-chat-text -->
              </div>
              <!-- /.direct-chat-msg -->
            <?php } else { ?>
              <!-- Message to the right -->
              <div class="direct-chat-msg right">
                <div class="direct-chat-info clearfix">
                  <span class="direct-chat-name pull-right">Anda</span>
                  <!-- <span class="direct-chat-timestamp pull-left"><?=$value->tanggal;?></span> -->
                </div>
                <!-- /.direct-chat-info -->
                <img class="direct-chat-img" src="<?=Yii::$app->request->baseUrl?>/images/no-person.jpg?>" alt="message user image">
                <!-- /.direct-chat-img -->
                <div class="direct-chat-text col-md-6 pull-right">
                  <div class="pull-right">
                    <div align="right">
                      <?php 
                        if(!empty($value->dokumen)){
                        ?>
                        <i class="fa fa-file-word-o" style="color: black; font-size: 15pt;"></i> <a href="../media/file_mahasiswa/<?=$value->dokumen?>" style="color: black; text-decoration: underline;"><?=$value->dokumen?></a>
                        <br>
                        <?php
                            }
                        ?>
                        <?=$value->isi;?>
                    </div>
                  </div>
                  
                  <br>
                  <span class="direct-chat-timestamp pull-left" style="color: white; font-size: 8pt;"><?=$value->tanggal;?></span>
                </div>
                <!-- /.direct-chat-text -->
              </div>
              <!-- /.direct-chat-msg -->
            <?php } ?>
            <?php  
            }
          }
            
            // print_r($dataProvider->getModels());
          ?>
          <!-- Message. Default to the left -->
          <!-- <div class="direct-chat-msg">
            <div class="direct-chat-info clearfix">
              <span class="direct-chat-name pull-left">Alexander Pierce</span>
              <span class="direct-chat-timestamp pull-right">23 Jan 2:00 pm</span>
            </div> -->
            <!-- /.direct-chat-info -->
            <!-- <img class="direct-chat-img" src="dist/img/user1-128x128.jpg" alt="message user image"> -->
            <!-- /.direct-chat-img -->
            <!-- <div class="direct-chat-text">
              Is this template really for free? That's unbelievable!
            </div> -->
            <!-- /.direct-chat-text -->
          <!-- </div> -->
          <!-- /.direct-chat-msg -->

          

        </div>
        <!--/.direct-chat-messages-->

        <!-- Contacts are loaded here -->
        <div class="direct-chat-contacts">
          <ul class="contacts-list">
            <li>
              <a href="#">
                <img class="contacts-list-img" src="dist/img/user1-128x128.jpg" alt="User Image">

                <div class="contacts-list-info">
                      <span class="contacts-list-name">
                        Count Dracula
                        <small class="contacts-list-date pull-right">2/28/2015</small>
                      </span>
                  <span class="contacts-list-msg">How have you been? I was...</span>
                </div>
                <!-- /.contacts-list-info -->
              </a>
            </li>
            <!-- End Contact Item -->
            <li>
              <a href="#">
                <img class="contacts-list-img" src="dist/img/user7-128x128.jpg" alt="User Image">

                <div class="contacts-list-info">
                      <span class="contacts-list-name">
                        Sarah Doe
                        <small class="contacts-list-date pull-right">2/23/2015</small>
                      </span>
                  <span class="contacts-list-msg">I will be waiting for...</span>
                </div>
                <!-- /.contacts-list-info -->
              </a>
            </li>
            <!-- End Contact Item -->
            <li>
              <a href="#">
                <img class="contacts-list-img" src="dist/img/user3-128x128.jpg" alt="User Image">

                <div class="contacts-list-info">
                      <span class="contacts-list-name">
                        Nadia Jolie
                        <small class="contacts-list-date pull-right">2/20/2015</small>
                      </span>
                  <span class="contacts-list-msg">I'll call you back at...</span>
                </div>
                <!-- /.contacts-list-info -->
              </a>
            </li>
            <!-- End Contact Item -->
            <li>
              <a href="#">
                <img class="contacts-list-img" src="dist/img/user5-128x128.jpg" alt="User Image">

                <div class="contacts-list-info">
                      <span class="contacts-list-name">
                        Nora S. Vans
                        <small class="contacts-list-date pull-right">2/10/2015</small>
                      </span>
                  <span class="contacts-list-msg">Where is your new...</span>
                </div>
                <!-- /.contacts-list-info -->
              </a>
            </li>
            <!-- End Contact Item -->
            <li>
              <a href="#">
                <img class="contacts-list-img" src="dist/img/user6-128x128.jpg" alt="User Image">

                <div class="contacts-list-info">
                      <span class="contacts-list-name">
                        John K.
                        <small class="contacts-list-date pull-right">1/27/2015</small>
                      </span>
                  <span class="contacts-list-msg">Can I take a look at...</span>
                </div>
                <!-- /.contacts-list-info -->
              </a>
            </li>
            <!-- End Contact Item -->
            <li>
              <a href="#">
                <img class="contacts-list-img" src="dist/img/user8-128x128.jpg" alt="User Image">

                <div class="contacts-list-info">
                      <span class="contacts-list-name">
                        Kenneth M.
                        <small class="contacts-list-date pull-right">1/4/2015</small>
                      </span>
                  <span class="contacts-list-msg">Never mind I found...</span>
                </div>
                <!-- /.contacts-list-info -->
              </a>
            </li>
            <!-- End Contact Item -->
          </ul>
          <!-- /.contatcts-list -->
        </div>
        <!-- /.direct-chat-pane -->
      </div>
      <!-- /.box-body -->
      <div class="box-footer">
        <?php $form = ActiveForm::begin(); ?>
          <div class="col-md-12">
            <?= $form->field($model, 'dokumen')->fileInput() ?>

            <?= $form->field($model, 'isi')->textarea(['rows' => 6,'class'=>'form-control', 'placeholder' => "Ketik Pesan"])->label(false) ?>
            <!-- <input type="text" name="message" placeholder="Ketik Pesan" class="form-control"> -->
            
          </div>
          <div class="col-md-12" style="text-align: right;">
            <span class="input-group-btn">
              <?= Html::submitButton('Kirim', ['class' => 'btn btn-warning btn-flat']) ?>
                 <!--  <button type="button" class="btn btn-warning btn-flat">Kirim</button> -->
                </span>
          </div>
       <?php ActiveForm::end(); ?>
      </div>
      <!-- /.box-footer-->
    </div>
    <!--/.direct-chat -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->