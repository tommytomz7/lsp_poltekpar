<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use budyaga\cropper\Widget;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\UploadApl01 */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="upload-apl01-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'portopolio')->fileInput() ?>

    <?= $form->field($model, 'pas_photo')->fileInput() ?>
    
    <?= $form->field($model, 'ktm_ktp')->fileInput() ?>

    <?= $form->field($model, 'ijazah')->fileInput() ?>

    <?= $form->field($model, 'sertifikat_pkl')->fileInput() ?>


	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Tambah' : 'Ubah', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
