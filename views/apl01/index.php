<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;
use yii\widgets\ActiveForm;
use app\models\TandaPengenal;
use yii\helpers\ArrayHelper;

$datatandapengenal=ArrayHelper::map(TandaPengenal::find()->all(),'id','nama');
?>

<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
<style type="text/css">
    .timeline {
        list-style-type: none;
        display: flex;
        align-items: center;
        justify-content: center;
    }

    .li {
        transition: all 200ms ease-in;
    }

    .timestamp {
        margin-bottom: 20px;
        padding: 0px 40px;
        display: flex;
        flex-direction: column;
        align-items: center;
        font-weight: 100;
        text-align: center;
        font-size: 11px;
        font-we
    }

    .status {
        /* padding: 0px 40px; */
        padding: 0px 27px;
        display: flex;
        justify-content: center;
        border-top: 2px solid #D6DCE0;
        position: relative;
        transition: all 200ms ease-in;
    }

    .status h4 {
        font-weight: 600;
        font-size: 11px;
        padding-top: 16px;
        text-align: center;
    }

    .status:before {
        content: "";
        width: 25px;
        height: 25px;
        background-color: white;
        border-radius: 25px;
        border: 1px solid #ddd;
        position: absolute;
        top: -15px;
        left: 42%;
        transition: all 200ms ease-in;
    }

    .li.complete .status {
        border-top: 2px solid #66DC71;
    }

    .li.complete .status:before {
        background-color: #66DC71;
        border: none;
        transition: all 200ms ease-in;
    }

    .li.complete .status h4 {
        color: #66DC71;
    }

    @media (min-device-width: 320px) and (max-device-width: 700px) {
        .timeline {
            list-style-type: none;
            display: block;
        }

        .li {
            transition: all 200ms ease-in;
            display: flex;
            width: inherit;
        }

        .timestamp {
            width: 100px;
        }

        .status:before {
            left: -8%;
            top: 30%;
            transition: all 200ms ease-in;
        }
    }

    /* html, body {
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  font-family: "Titillium Web", sans serif;
  color: #758D96;
} */

    /* button {
  position: absolute;
  width: 100px;
  min-width: 100px;
  padding: 20px;
  margin: 20px;
  font-family: "Titillium Web", sans serif;
  border: none;
  color: white;
  font-size: 16px;
  text-align: center;
} */

    #toggleButton {
        position: absolute;
        left: 50px;
        top: 20px;
        background-color: #75C7F6;
    }
</style>

<div class="about">
    <div class="container_width">
    <?php
    // if(!isset($_GET['step_registrasi'])){
    //     $step_registrasi = 0;
    // }else{
    //     $step_registrasi = $_GET['step_registrasi'];
    // }
    $step_registrasi = $step;
        // $id_registrasi = Yii::$app->user->identity->id_registrasi;
        // $id_user = ;
        $step = [
            1 => "Skema Sertifikasi",
            2 => "APL 01",
            3 => "Selesai Registrasi"
        ];
        ?>

        <div class="titlepage text_align_left">
            <h2>APL01</h2>
            <p></p>
        </div>
        

        <ul class="timeline" id="timeline">
            <?php
            for ($x = 1; $x <= count($step); $x++) {
                $status = '';

                $ket = '';
                if ($x <= $step_registrasi) {
                    $status = 'complete';
                    $ket =  "Selesai";
                } else {
                    $ket =  "Belum Selesai";
                }
                
                ?>
                <li class='li <?= $status ?> '>
                    <div class="timestamp">
                        <span class="author"> <b><?= $step[$x] ?></b></span>
                    </div>
                    <div class="status">
                        <h4> <?= $ket ?> </h4>
                    </div>
                </li>

            <?php } 
            
            if($x==3){
                $status = 'complete';
                $ket =  "Selesai";
            }
        ?>

        </ul>

        <br>
        <div class="container">
            <?php if($step_registrasi==0){ ?>
                <div id="ajaxCrudDatatable">
                    <?=GridView::widget([
                        'id'=>'crud-datatable',
                        'dataProvider' => $dataProvider,
                        // 'filterModel' => $searchModel,
                        'pjax'=>true,
                        'summary' => '',
                        'columns' => require(__DIR__.'/_columns_unit_sertifikasi.php'),
                        // 'toolbar'=> [
                        //     ['content'=>
                        //         Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'],
                        //         ['role'=>'modal-remote','title'=> 'Tambah Unit Sertfikasi','class'=>'btn btn-success']).
                        //         Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
                        //         ['data-pjax'=>1, 'class'=>'btn btn-warning', 'title'=>'Reset Grid']).
                        //         '{toggleData}'.
                        //         '{export}'
                        //     ],
                        // ],          
                        'striped' => true,
                        'condensed' => true,
                        'responsive' => true,          
                        // 'panel' => [
                        //     'type' => 'primary', 
                        //     'heading' => '<i class="glyphicon glyphicon-list"></i> Daftar Unit Sertifikasi',
                        //     //'before'=>'<em>* Resize table columns just like a spreadsheet by dragging the column edges.</em>',
                        //     'after'=>BulkButtonWidget::widget([
                        //                 'buttons'=>Html::a('<i class="glyphicon glyphicon-trash"></i>&nbsp; Hapus Semua',
                        //                     ["bulk-delete"] ,
                        //                     [
                        //                         "class"=>"btn btn-danger btn-xs",
                        //                         'role'=>'modal-remote-bulk',
                        //                         'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                        //                         'data-request-method'=>'post',
                        //                         'data-confirm-title'=>'Anda Yakin?',
                        //                         'data-confirm-message'=>'Apakah ingin menghapus data ini'
                        //                     ]),
                        //             ]).                        
                        //             '<div class="clearfix"></div>',
                        // ]
                    ])?>
                </div>
            <?php } ?>

            <?php if($step_registrasi==1){ ?>
                <?php $form = ActiveForm::begin(); ?>

                <?= $form->field($model, 'idunitsertifikasi')->hiddenInput()->label(false) ?>

                <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'idtandapengenal')
                    ->dropDownList(
                        $datatandapengenal,           // Flat array ('id'=>'label')
                        ['prompt'=>'Pilih']    // options
                    );
                ?>

                <?= $form->field($model, 'tempat_lahir')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'tanggal_lahir')->textInput() ?>

                <?= $form->field($model, 'jenis_kelamin')
                    ->dropDownList(
                        ['Laki-laki'=>'Laki-laki', 'Perempuan'=>'Perempuan'],           // Flat array ('id'=>'label')
                        ['prompt'=>'Pilih']    // options
                    );
                ?>
                
                <?= $form->field($model, 'kebangsaan')
                    ->dropDownList(
                        ['WNI'=>'WNI', 'WNA'=>'WNA'],           // Flat array ('id'=>'label')
                        ['prompt'=>'Pilih']    // options
                    );
                ?>

                <?= $form->field($model, 'alamat_rumah')->textarea(['rows' => 6]) ?>

                <?= $form->field($model, 'kodepos')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'telp')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'pendidikan')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'perusahaan')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'jabatan')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'alamat_kantor')->textarea(['rows' => 6]) ?>

                <?= $form->field($model, 'kodepos_kantor')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'telp_kantor')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'email_kantor')->textInput(['maxlength' => true]) ?>

                <?= $form->field($modeluploadapl01, 'portopolio')->fileInput() ?>

                <?= $form->field($modeluploadapl01, 'pas_photo')->fileInput() ?>

                <?= $form->field($modeluploadapl01, 'ktm_ktp')->fileInput() ?>

                <?= $form->field($modeluploadapl01, 'ijazah')->fileInput() ?>

                <?= $form->field($modeluploadapl01, 'sertifikat_pkl')->fileInput() ?>
                
                <?php if (!Yii::$app->request->isAjax){ ?>
                    <div class="form-group">
                        <?= Html::submitButton($model->isNewRecord ? 'Proses' : 'Ubah', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    </div>
                <?php } ?>

                <?php ActiveForm::end(); ?>
            <?php } ?>

            <?php if($step_registrasi==2){ ?>
                <?php if (Yii::$app->session->hasFlash('success')): ?>
                    <div class="alert alert-success alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <h4><i class="icon fa fa-check"></i>Saved!</h4>
                        <?= Yii::$app->session->getFlash('success') ?>
                    </div>
                <?php endif; ?>

            <?php } ?>
        </div>
        <?php
        // $wizard_config = [
        //     'id' => 'stepwizard',
        //     'steps' => [
        //         1 => [
        //             'title' => 'Step 1',
        //             'icon' => 'glyphicon glyphicon-cloud-download',
        //             'content' => '<h3>Step 1</h3>This is step 1',
        //             'buttons' => [
        //                 'next' => [
        //                     'title' => 'Forward', 
        //                     'options' => [
        //                         'class' => 'disabled'
        //                     ],
        //                 ],
        //             ],
        //         ],
        //         2 => [
        //             'title' => 'Step 2',
        //             'icon' => 'glyphicon glyphicon-cloud-upload',
        //             'content' => '<h3>Step 2</h3>This is step 2',
        //             'skippable' => true,
        //         ],
        //         3 => [
        //             'title' => 'Step 3',
        //             'icon' => 'glyphicon glyphicon-transfer',
        //             'content' => '<h3>Step 3</h3>This is step 3',
        //         ],
        //     ],
        //     'complete_content' => "You are done!", // Optional final screen
        //     'start_step' => 2, // Optional, start with a specific step
        // ];
        ?>

        
    </div>
</div>

