<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\models\SkemaSertifikasi;
use app\models\JenisStandar;

/* @var $this yii\web\View */
/* @var $model app\models\UnitSertfikasi */
/* @var $form yii\widgets\ActiveForm */

$dataskema=ArrayHelper::map(SkemaSertifikasi::find()->all(),'id','judul_skema');
$datajenisstandar=ArrayHelper::map(JenisStandar::find()->all(),'id','nama');

?>

<div class="unit-sertfikasi-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idskemasertifikasi')->widget(Select2::classname(), [
        'data' => $dataskema,
        'options' => ['placeholder' => 'Pilih'],
        'pluginOptions' => [
            'allowClear' => false,
        ],
    ]); ?>

    <?= $form->field($model, 'kode_unit')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'judul_unit')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'idjenisstandar')->widget(Select2::classname(), [
        'data' => $datajenisstandar,
        'options' => ['placeholder' => 'Pilih'],
        'pluginOptions' => [
            'allowClear' => false,
        ],
    ]); ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Tambah' : 'Ubah', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
