<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Apl01 */
?>
<div class="apl01-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
