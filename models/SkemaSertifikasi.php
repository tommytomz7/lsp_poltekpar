<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "skema_sertifikasi".
 *
 * @property int $id
 * @property string $judul_skema
 *
 * @property UnitSertfikasi[] $unitSertfikasis
 */
class SkemaSertifikasi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'skema_sertifikasi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['judul_skema'], 'required'],
            [['judul_skema'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'judul_skema' => 'Judul Skema',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnitSertfikasis()
    {
        return $this->hasMany(UnitSertfikasi::className(), ['idskemasertifikasi' => 'id']);
    }
}
