<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Apl01 */
?>
<div class="apl01-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'idunitsertifikasi',
            'nama',
            'idtandapengenal',
            'tempat_lahir',
            'tanggal_lahir',
            'jenis_kelamin',
            'kebangsaan',
            'alamat_rumah:ntext',
            'kodepos',
            'telp',
            'email:email',
            'pendidikan',
            'perusahaan',
            'jabatan',
            'alamat_kantor:ntext',
            'kodepos_kantor',
            'telp_kantor',
            'email_kantor:email',
            'status',
            'tanggal',
        ],
    ]) ?>

</div>
