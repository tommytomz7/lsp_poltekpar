<?php

namespace app\models\searchs;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\UnitSertfikasi;

/**
 * UnitSertfikasiSearch represents the model behind the search form about `app\models\UnitSertfikasi`.
 */
class UnitSertfikasiSearch extends UnitSertfikasi
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'idskemasertifikasi', 'idjenisstandar'], 'integer'],
            [['kode_unit', 'judul_unit'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UnitSertfikasi::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'idskemasertifikasi' => $this->idskemasertifikasi,
            'idjenisstandar' => $this->idjenisstandar,
        ]);

        $query->andFilterWhere(['like', 'kode_unit', $this->kode_unit])
            ->andFilterWhere(['like', 'judul_unit', $this->judul_unit]);

        return $dataProvider;
    }
}
