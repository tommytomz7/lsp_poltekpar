<?php

namespace app\models\searchs;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Apl01;

/**
 * Apl01Search represents the model behind the search form about `app\models\Apl01`.
 */
class Apl01Search extends Apl01
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'idunitsertifikasi', 'idtandapengenal', 'status'], 'integer'],
            [['nama', 'tempat_lahir', 'tanggal_lahir', 'jenis_kelamin', 'kebangsaan', 'alamat_rumah', 'kodepos', 'telp', 'email', 'pendidikan', 'perusahaan', 'jabatan', 'alamat_kantor', 'kodepos_kantor', 'telp_kantor', 'email_kantor', 'tanggal'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Apl01::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'idunitsertifikasi' => $this->idunitsertifikasi,
            'idtandapengenal' => $this->idtandapengenal,
            'tanggal_lahir' => $this->tanggal_lahir,
            'status' => $this->status,
            'tanggal' => $this->tanggal,
        ]);

        $query->andFilterWhere(['like', 'nama', $this->nama])
            ->andFilterWhere(['like', 'tempat_lahir', $this->tempat_lahir])
            ->andFilterWhere(['like', 'jenis_kelamin', $this->jenis_kelamin])
            ->andFilterWhere(['like', 'kebangsaan', $this->kebangsaan])
            ->andFilterWhere(['like', 'alamat_rumah', $this->alamat_rumah])
            ->andFilterWhere(['like', 'kodepos', $this->kodepos])
            ->andFilterWhere(['like', 'telp', $this->telp])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'pendidikan', $this->pendidikan])
            ->andFilterWhere(['like', 'perusahaan', $this->perusahaan])
            ->andFilterWhere(['like', 'jabatan', $this->jabatan])
            ->andFilterWhere(['like', 'alamat_kantor', $this->alamat_kantor])
            ->andFilterWhere(['like', 'kodepos_kantor', $this->kodepos_kantor])
            ->andFilterWhere(['like', 'telp_kantor', $this->telp_kantor])
            ->andFilterWhere(['like', 'email_kantor', $this->email_kantor]);

        return $dataProvider;
    }
}
