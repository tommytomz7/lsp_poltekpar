<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'LOGIN';

$fieldOptions1 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-envelope form-control-feedback'></span>"
];

$fieldOptions2 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-lock form-control-feedback'></span>"
];
?>

<style>
    .Absolute-Center.is-Responsive {
    width: 50%; 
    height: 50%;
    min-width: 200px;
    max-width: 400px;
    padding: 40px;
    }
</style>

<div class="about">
    <div class="container_width">
        <div class="titlepage text_align_center">
            <h2>Login</h2>
            <p></p>
        </div>
        <div style="width:50%;" class="text_align_center">
            
            <center><img src="<?=Yii::$app->request->baseUrl?>/images/logo_akpar50.png"></center><br>
        <!--  <p class="login-box-msg"><b>SISTEM INFORMASI KEUANGAN</b></p> -->

            <?php $form = ActiveForm::begin(['id' => 'login-form', 'enableClientValidation' => false]); ?>

            <?= $form
                ->field($model, 'username', $fieldOptions1)
                ->label(false)
                ->textInput(['placeholder' => $model->getAttributeLabel('username')]) ?>

            <?= $form
                ->field($model, 'password', $fieldOptions2)
                ->label(false)
                ->passwordInput(['placeholder' => $model->getAttributeLabel('password')]) ?>

            <div class="row">
                <div class="col-xs-8">
                    <?= $form->field($model, 'rememberMe')->checkbox() ?>
                </div>
                <!-- /.col -->
                <div class="col-xs-4" style="margin-top: 10px;">
                    <?= Html::a('Registrasi','?r=site/registrasi-mahasiswa') ?>
                </div>

                
                <!-- /.col -->
                
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <?= Html::submitButton('Login', ['class' => 'btn btn-primary btn-block btn-flat', 'n1me' => 'login-button']) ?>
                </div>
            </div>
        
            <?php ActiveForm::end(); ?>

            <!--<div class="social-auth-links text-center">
                <p>- OR -</p>
                <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in
                    using Facebook</a>
                <a href="#" class="btn btn-block btn-social btn-google-plus btn-flat"><i class="fa fa-google-plus"></i> Sign
                    in using Google+</a>
            </div>-->
            
            <!-- /.social-auth-links -->

            <!--<a href="#">I forgot my password</a><br>
            <a href="register.html" class="text-center">Register a new membership</a>-->

        </div>
    </div>
</div>


