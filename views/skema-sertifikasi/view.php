<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\SkemaSertifikasi */
?>
<div class="skema-sertifikasi-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'judul_skema',
        ],
    ]) ?>

</div>
