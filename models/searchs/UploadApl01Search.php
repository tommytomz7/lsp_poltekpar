<?php

namespace app\models\searchs;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\UploadApl01;

/**
 * UploadApl01Search represents the model behind the search form about `app\models\UploadApl01`.
 */
class UploadApl01Search extends UploadApl01
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'idapl01'], 'integer'],
            [['portopolio', 'pas_photo', 'ktm_ktp', 'ijazah', 'sertifikat_pkl', 'tanggal'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UploadApl01::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'idapl01' => $this->idapl01,
            'tanggal' => $this->tanggal,
        ]);

        $query->andFilterWhere(['like', 'portopolio', $this->portopolio])
            ->andFilterWhere(['like', 'pas_photo', $this->pas_photo])
            ->andFilterWhere(['like', 'ktm_ktp', $this->ktm_ktp])
            ->andFilterWhere(['like', 'ijazah', $this->ijazah])
            ->andFilterWhere(['like', 'sertifikat_pkl', $this->sertifikat_pkl]);

        return $dataProvider;
    }
}
