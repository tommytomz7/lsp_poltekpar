<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Mahasiswa */
?>
<div class="mahasiswa-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'nim',
            'nama',
            'jenis_kelamin',
            'idprogramstudi',
            'tanggal',
        ],
    ]) ?>

</div>
