<?php
use yii\helpers\Html;
use yii\bootstrap\Modal;
use johnitvn\ajaxcrud\CrudAsset; 
/* @var $this \yii\web\View */
/* @var $content string */

CrudAsset::register($this);
?>

<header class="header-area">
    <div class="left">
        <a href="Javascript:void(0)"><i class="fa fa-search" aria-hidden="true"></i></a>
    </div>
    <div class="right">
        <a href="Javascript:void(0)"><i class="fa fa-user" aria-hidden="true"></i></a>
    </div>
    <div class="container">
        <div class="row d_flex">
            <div class="col-sm-3 logo_sm">
                <div class="logo">
                <a href="index.html"></a>
                </div>
            </div>
            <div class="col-lg-10 offset-lg-1 col-md-12 col-sm-9">
                <div class="navbar-area">
                <nav class="site-navbar">
                    <ul>
                        <li><a class="" href="<?=Yii::$app->request->baseUrl?>">Home</a></li>
                        <li><a href="?r=mahasiswa/create">Registrasi</a></li>
                        <li><a href="?r=apl01">APL01</a></li>
                        <li><a href="" class="logo_midle">LSP POLTEKPAR</a></li>
                        <li><a href="news.html">news</a></li>
                        <li><a href="doctores.html">doctores</a></li>
                        <?php if (isset(Yii::$app->session->id)) { ?>
                            <li><a href="?r=site/logout">LOGOUT</a></li>
                        <?php } else {?>
                            <li><a href="?r=site/login">LOGIN</a></li>
                        <?php } ?>
                    </ul>
                    <button class="nav-toggler">
                    <span></span>
                    </button>
                </nav>
                </div>
            </div>
        </div>
    </div>
</header>