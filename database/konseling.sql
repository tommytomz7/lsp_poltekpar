-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 28, 2021 at 05:23 AM
-- Server version: 8.0.15
-- PHP Version: 7.2.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `konseling`
--
CREATE DATABASE IF NOT EXISTS `konseling` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `konseling`;

DELIMITER $$
--
-- Functions
--
DROP FUNCTION IF EXISTS `prosesBimbingan`$$
CREATE DEFINER=`root`@`localhost` FUNCTION `prosesBimbingan` () RETURNS TINYINT(4) BEGIN
	declare hasil tinyint default 0;

	insert into lulus_bimbingan (id_mahasiswa, semester, jumlah_bimbingan)
	select 
		distinct
		mh.id,
		mh.semester,
		(
											select 
													count(1) 
											from bimbingan bb 
											where bb.id_mahasiswa = bm.id_mahasiswa 
											and status = 1
									)
	from bimbingan bm
	inner join mahasiswa mh on bm.id_mahasiswa = mh.id
	where not exists(
		select 1 from lulus_bimbingan lb where bm.id_mahasiswa = lb.id_mahasiswa and mh.semester = lb.semester
	) and 
	(
											select 
													count(1) 
											from bimbingan bb 
											where bb.id_mahasiswa = bm.id_mahasiswa 
											and status = 1
									) >= 8;

	
	insert into tidak_lulus_bimbingan (id_mahasiswa, semester, jumlah_bimbingan)
	select 
		distinct
		mh.id,
		mh.semester,
		(
											select 
													count(1) 
											from bimbingan bb 
											where bb.id_mahasiswa = bm.id_mahasiswa 
											and status = 1
									)
	from bimbingan bm
	inner join mahasiswa mh on bm.id_mahasiswa = mh.id
	where not exists(
		select 1 from tidak_lulus_bimbingan lb where bm.id_mahasiswa = lb.id_mahasiswa and mh.semester = lb.semester
	) and 
	(
											select 
													count(1) 
											from bimbingan bb 
											where bb.id_mahasiswa = bm.id_mahasiswa 
											and status = 1
									) < 8;
	set hasil =1;

	RETURN hasil;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `admin_layanan`
--

DROP TABLE IF EXISTS `admin_layanan`;
CREATE TABLE `admin_layanan` (
  `id` int(11) NOT NULL,
  `nip` varchar(20) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `idjabatan` int(11) NOT NULL,
  `foto` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin_layanan`
--

INSERT INTO `admin_layanan` (`id`, `nip`, `nama`, `idjabatan`, `foto`) VALUES
(7, '345678', 'robby', 1, '../media/foto/5ff1d4239ee44.png');

-- --------------------------------------------------------

--
-- Table structure for table `admin_mahasiswa`
--

DROP TABLE IF EXISTS `admin_mahasiswa`;
CREATE TABLE `admin_mahasiswa` (
  `id` int(11) NOT NULL,
  `nip` varchar(25) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `idprodi` int(255) NOT NULL,
  `idjabatan` int(11) NOT NULL,
  `foto` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin_mahasiswa`
--

INSERT INTO `admin_mahasiswa` (`id`, `nip`, `nama`, `idprodi`, `idjabatan`, `foto`) VALUES
(4, '65444444', 'Putri', 1, 2, '../media/foto/6038c66abff57.png'),
(5, '999900001', 'Admin Sukaati', 2, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_prodi`
--

DROP TABLE IF EXISTS `admin_prodi`;
CREATE TABLE `admin_prodi` (
  `id` int(11) NOT NULL,
  `nip` varchar(25) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `idprodi` int(255) NOT NULL,
  `idjabatan` int(11) NOT NULL,
  `foto` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin_prodi`
--

INSERT INTO `admin_prodi` (`id`, `nip`, `nama`, `idprodi`, `idjabatan`, `foto`) VALUES
(2, '21210001', 'Mursid', 1, 1, '../media/foto/602e8428c870a.png');

-- --------------------------------------------------------

--
-- Table structure for table `auth_assignment`
--

DROP TABLE IF EXISTS `auth_assignment`;
CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_assignment`
--

INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
('Admin Layanan', '15', 1602910578),
('Admin Layanan', '16', 1602910610),
('Admin Layanan', '17', 1602910677),
('Admin Layanan', '18', 1602910809),
('Admin Layanan', '20', 1603118085),
('Admin Mahasiswa', '32', 1614328680),
('Admin Mahasiswa', '33', 1614407441),
('Developer', '1', 1574934123),
('Developer', '31', 1613898492),
('Dosen', '23', 1609388982),
('Dosen', '24', 1609389238),
('Dosen', '25', 1609685377),
('Dosen', '27', 1612582501),
('Mahasiswa', '13', 1602906747),
('Mahasiswa', '14', 1602907270),
('Mahasiswa', '19', 1602910941),
('Mahasiswa', '21', 1603291651),
('Mahasiswa', '22', 1609160870),
('Mahasiswa', '26', 1611061006),
('Program Studi', '28', 1613641887);

-- --------------------------------------------------------

--
-- Table structure for table `auth_item`
--

DROP TABLE IF EXISTS `auth_item`;
CREATE TABLE `auth_item` (
  `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `type` smallint(6) NOT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_item`
--

INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
('/*', 2, NULL, NULL, NULL, 1574934094, 1574934094),
('/admin-layanan/*', 2, NULL, NULL, NULL, 1602907572, 1602907572),
('/admin-layanan/bulk-delete', 2, NULL, NULL, NULL, 1602907572, 1602907572),
('/admin-layanan/create', 2, NULL, NULL, NULL, 1602907572, 1602907572),
('/admin-layanan/delete', 2, NULL, NULL, NULL, 1602907572, 1602907572),
('/admin-layanan/index', 2, NULL, NULL, NULL, 1602907572, 1602907572),
('/admin-layanan/ubah-profil', 2, NULL, NULL, NULL, 1609683525, 1609683525),
('/admin-layanan/update', 2, NULL, NULL, NULL, 1602907572, 1602907572),
('/admin-layanan/view', 2, NULL, NULL, NULL, 1602907572, 1602907572),
('/admin-mahasiswa/*', 2, NULL, NULL, NULL, 1614328007, 1614328007),
('/admin-mahasiswa/bulk-delete', 2, NULL, NULL, NULL, 1614328007, 1614328007),
('/admin-mahasiswa/create', 2, NULL, NULL, NULL, 1614328007, 1614328007),
('/admin-mahasiswa/delete', 2, NULL, NULL, NULL, 1614328007, 1614328007),
('/admin-mahasiswa/index', 2, NULL, NULL, NULL, 1614328007, 1614328007),
('/admin-mahasiswa/ubah-profil', 2, NULL, NULL, NULL, 1614331309, 1614331309),
('/admin-mahasiswa/update', 2, NULL, NULL, NULL, 1614328007, 1614328007),
('/admin-mahasiswa/view', 2, NULL, NULL, NULL, 1614328007, 1614328007),
('/admin-prodi/*', 2, NULL, NULL, NULL, 1613637083, 1613637083),
('/admin-prodi/bulk-delete', 2, NULL, NULL, NULL, 1613637082, 1613637082),
('/admin-prodi/create', 2, NULL, NULL, NULL, 1613637082, 1613637082),
('/admin-prodi/delete', 2, NULL, NULL, NULL, 1613637082, 1613637082),
('/admin-prodi/index', 2, NULL, NULL, NULL, 1613637082, 1613637082),
('/admin-prodi/ubah-profil', 2, NULL, NULL, NULL, 1613660866, 1613660866),
('/admin-prodi/update', 2, NULL, NULL, NULL, 1613637082, 1613637082),
('/admin-prodi/view', 2, NULL, NULL, NULL, 1613637082, 1613637082),
('/admin/*', 2, NULL, NULL, NULL, 1574934093, 1574934093),
('/admin/assignment/*', 2, NULL, NULL, NULL, 1574934093, 1574934093),
('/admin/assignment/assign', 2, NULL, NULL, NULL, 1574934093, 1574934093),
('/admin/assignment/index', 2, NULL, NULL, NULL, 1574934093, 1574934093),
('/admin/assignment/revoke', 2, NULL, NULL, NULL, 1574934093, 1574934093),
('/admin/assignment/view', 2, NULL, NULL, NULL, 1574934093, 1574934093),
('/admin/default/*', 2, NULL, NULL, NULL, 1574934093, 1574934093),
('/admin/default/index', 2, NULL, NULL, NULL, 1574934093, 1574934093),
('/admin/menu/*', 2, NULL, NULL, NULL, 1574934093, 1574934093),
('/admin/menu/create', 2, NULL, NULL, NULL, 1574934093, 1574934093),
('/admin/menu/delete', 2, NULL, NULL, NULL, 1574934093, 1574934093),
('/admin/menu/index', 2, NULL, NULL, NULL, 1574934093, 1574934093),
('/admin/menu/update', 2, NULL, NULL, NULL, 1574934093, 1574934093),
('/admin/menu/view', 2, NULL, NULL, NULL, 1574934093, 1574934093),
('/admin/permission/*', 2, NULL, NULL, NULL, 1574934093, 1574934093),
('/admin/permission/assign', 2, NULL, NULL, NULL, 1574934093, 1574934093),
('/admin/permission/create', 2, NULL, NULL, NULL, 1574934093, 1574934093),
('/admin/permission/delete', 2, NULL, NULL, NULL, 1574934093, 1574934093),
('/admin/permission/index', 2, NULL, NULL, NULL, 1574934093, 1574934093),
('/admin/permission/remove', 2, NULL, NULL, NULL, 1574934093, 1574934093),
('/admin/permission/update', 2, NULL, NULL, NULL, 1574934093, 1574934093),
('/admin/permission/view', 2, NULL, NULL, NULL, 1574934093, 1574934093),
('/admin/role/*', 2, NULL, NULL, NULL, 1574934093, 1574934093),
('/admin/role/assign', 2, NULL, NULL, NULL, 1574934093, 1574934093),
('/admin/role/create', 2, NULL, NULL, NULL, 1574934093, 1574934093),
('/admin/role/delete', 2, NULL, NULL, NULL, 1574934093, 1574934093),
('/admin/role/index', 2, NULL, NULL, NULL, 1574934093, 1574934093),
('/admin/role/remove', 2, NULL, NULL, NULL, 1574934093, 1574934093),
('/admin/role/update', 2, NULL, NULL, NULL, 1574934093, 1574934093),
('/admin/role/view', 2, NULL, NULL, NULL, 1574934093, 1574934093),
('/admin/route/*', 2, NULL, NULL, NULL, 1574934093, 1574934093),
('/admin/route/assign', 2, NULL, NULL, NULL, 1574934093, 1574934093),
('/admin/route/create', 2, NULL, NULL, NULL, 1574934093, 1574934093),
('/admin/route/index', 2, NULL, NULL, NULL, 1574934093, 1574934093),
('/admin/route/refresh', 2, NULL, NULL, NULL, 1574934093, 1574934093),
('/admin/route/remove', 2, NULL, NULL, NULL, 1574934093, 1574934093),
('/admin/rule/*', 2, NULL, NULL, NULL, 1574934093, 1574934093),
('/admin/rule/create', 2, NULL, NULL, NULL, 1574934093, 1574934093),
('/admin/rule/delete', 2, NULL, NULL, NULL, 1574934093, 1574934093),
('/admin/rule/index', 2, NULL, NULL, NULL, 1574934093, 1574934093),
('/admin/rule/update', 2, NULL, NULL, NULL, 1574934093, 1574934093),
('/admin/rule/view', 2, NULL, NULL, NULL, 1574934093, 1574934093),
('/admin/user/*', 2, NULL, NULL, NULL, 1574934093, 1574934093),
('/admin/user/activate', 2, NULL, NULL, NULL, 1574934093, 1574934093),
('/admin/user/change-password', 2, NULL, NULL, NULL, 1574934093, 1574934093),
('/admin/user/delete', 2, NULL, NULL, NULL, 1574934093, 1574934093),
('/admin/user/index', 2, NULL, NULL, NULL, 1574934093, 1574934093),
('/admin/user/login', 2, NULL, NULL, NULL, 1574934093, 1574934093),
('/admin/user/logout', 2, NULL, NULL, NULL, 1574934093, 1574934093),
('/admin/user/request-password-reset', 2, NULL, NULL, NULL, 1574934093, 1574934093),
('/admin/user/reset-password', 2, NULL, NULL, NULL, 1574934093, 1574934093),
('/admin/user/signup', 2, NULL, NULL, NULL, 1574934093, 1574934093),
('/admin/user/view', 2, NULL, NULL, NULL, 1574934093, 1574934093),
('/bimbingan/*', 2, NULL, NULL, NULL, 1609563514, 1609563514),
('/bimbingan/bulk-delete', 2, NULL, NULL, NULL, 1609563514, 1609563514),
('/bimbingan/create', 2, NULL, NULL, NULL, 1609563514, 1609563514),
('/bimbingan/data-ikut-ujian', 2, NULL, NULL, NULL, 1609651709, 1609651709),
('/bimbingan/data-kurang-bimbingan', 2, NULL, NULL, NULL, 1609653423, 1609653423),
('/bimbingan/data-mahasiswa', 2, NULL, NULL, NULL, 1609578331, 1609578331),
('/bimbingan/data-setuju', 2, NULL, NULL, NULL, 1609594109, 1609594109),
('/bimbingan/data-tolak', 2, NULL, NULL, NULL, 1609595210, 1609595210),
('/bimbingan/delete', 2, NULL, NULL, NULL, 1609563514, 1609563514),
('/bimbingan/index', 2, NULL, NULL, NULL, 1609563514, 1609563514),
('/bimbingan/setuju', 2, NULL, NULL, NULL, 1609582162, 1609582162),
('/bimbingan/update', 2, NULL, NULL, NULL, 1609563514, 1609563514),
('/bimbingan/view', 2, NULL, NULL, NULL, 1609563514, 1609563514),
('/bimbingan/view-mahasiswa', 2, NULL, NULL, NULL, 1614413982, 1614413982),
('/debug/*', 2, NULL, NULL, NULL, 1574934094, 1574934094),
('/debug/default/*', 2, NULL, NULL, NULL, 1574934093, 1574934093),
('/debug/default/db-explain', 2, NULL, NULL, NULL, 1574934093, 1574934093),
('/debug/default/download-mail', 2, NULL, NULL, NULL, 1574934093, 1574934093),
('/debug/default/index', 2, NULL, NULL, NULL, 1574934093, 1574934093),
('/debug/default/toolbar', 2, NULL, NULL, NULL, 1574934093, 1574934093),
('/debug/default/view', 2, NULL, NULL, NULL, 1574934093, 1574934093),
('/debug/user/*', 2, NULL, NULL, NULL, 1574934094, 1574934094),
('/debug/user/reset-identity', 2, NULL, NULL, NULL, 1574934093, 1574934093),
('/debug/user/set-identity', 2, NULL, NULL, NULL, 1574934093, 1574934093),
('/dosen/*', 2, NULL, NULL, NULL, 1609386744, 1609386744),
('/dosen/bulk-delete', 2, NULL, NULL, NULL, 1609386744, 1609386744),
('/dosen/create', 2, NULL, NULL, NULL, 1609386744, 1609386744),
('/dosen/delete', 2, NULL, NULL, NULL, 1609386744, 1609386744),
('/dosen/index', 2, NULL, NULL, NULL, 1609386744, 1609386744),
('/dosen/ubah-profil', 2, NULL, NULL, NULL, 1609658914, 1609658914),
('/dosen/update', 2, NULL, NULL, NULL, 1609386744, 1609386744),
('/dosen/view', 2, NULL, NULL, NULL, 1609386744, 1609386744),
('/gii/*', 2, NULL, NULL, NULL, 1574934094, 1574934094),
('/gii/default/*', 2, NULL, NULL, NULL, 1574934094, 1574934094),
('/gii/default/action', 2, NULL, NULL, NULL, 1574934094, 1574934094),
('/gii/default/diff', 2, NULL, NULL, NULL, 1574934094, 1574934094),
('/gii/default/index', 2, NULL, NULL, NULL, 1574934094, 1574934094),
('/gii/default/preview', 2, NULL, NULL, NULL, 1574934094, 1574934094),
('/gii/default/view', 2, NULL, NULL, NULL, 1574934094, 1574934094),
('/golongan/*', 2, NULL, NULL, NULL, 1609386745, 1609386745),
('/golongan/bulk-delete', 2, NULL, NULL, NULL, 1609386745, 1609386745),
('/golongan/create', 2, NULL, NULL, NULL, 1609386744, 1609386744),
('/golongan/delete', 2, NULL, NULL, NULL, 1609386745, 1609386745),
('/golongan/index', 2, NULL, NULL, NULL, 1609386744, 1609386744),
('/golongan/update', 2, NULL, NULL, NULL, 1609386745, 1609386745),
('/golongan/view', 2, NULL, NULL, NULL, 1609386744, 1609386744),
('/gridview/*', 2, NULL, NULL, NULL, 1580352171, 1580352171),
('/gridview/export/*', 2, NULL, NULL, NULL, 1580352171, 1580352171),
('/gridview/export/download', 2, NULL, NULL, NULL, 1580352171, 1580352171),
('/jabatan/*', 2, NULL, NULL, NULL, 1602907573, 1602907573),
('/jabatan/bulk-delete', 2, NULL, NULL, NULL, 1602907573, 1602907573),
('/jabatan/create', 2, NULL, NULL, NULL, 1602907572, 1602907572),
('/jabatan/delete', 2, NULL, NULL, NULL, 1602907573, 1602907573),
('/jabatan/index', 2, NULL, NULL, NULL, 1602907572, 1602907572),
('/jabatan/update', 2, NULL, NULL, NULL, 1602907573, 1602907573),
('/jabatan/view', 2, NULL, NULL, NULL, 1602907572, 1602907572),
('/lulus-bimbingan/index', 2, NULL, NULL, NULL, 1613491083, 1613491083),
('/lulus-bimbingan/proses-cek-bimbingan', 2, NULL, NULL, NULL, 1614485560, 1614485560),
('/lulus-bimbingan/view', 2, NULL, NULL, NULL, 1614409770, 1614409770),
('/mahasiswa/*', 2, NULL, NULL, NULL, 1602859372, 1602859372),
('/mahasiswa/bulk-delete', 2, NULL, NULL, NULL, 1602859372, 1602859372),
('/mahasiswa/create', 2, NULL, NULL, NULL, 1602859372, 1602859372),
('/mahasiswa/delete', 2, NULL, NULL, NULL, 1602859372, 1602859372),
('/mahasiswa/index', 2, NULL, NULL, NULL, 1602859370, 1602859370),
('/mahasiswa/ubah-profil', 2, NULL, NULL, NULL, 1609390634, 1609390634),
('/mahasiswa/update', 2, NULL, NULL, NULL, 1602859372, 1602859372),
('/mahasiswa/view', 2, NULL, NULL, NULL, 1602859371, 1602859371),
('/percakapan-layanan-adma/*', 2, NULL, NULL, NULL, 1614331195, 1614331195),
('/percakapan-layanan-adma/bulk-delete', 2, NULL, NULL, NULL, 1614331195, 1614331195),
('/percakapan-layanan-adma/chat-adma', 2, NULL, NULL, NULL, 1614333100, 1614333100),
('/percakapan-layanan-adma/create', 2, NULL, NULL, NULL, 1614331195, 1614331195),
('/percakapan-layanan-adma/delete', 2, NULL, NULL, NULL, 1614331195, 1614331195),
('/percakapan-layanan-adma/index', 2, NULL, NULL, NULL, 1614331195, 1614331195),
('/percakapan-layanan-adma/update', 2, NULL, NULL, NULL, 1614331195, 1614331195),
('/percakapan-layanan-adma/view', 2, NULL, NULL, NULL, 1614331195, 1614331195),
('/percakapan-layanan-dosen/*', 2, NULL, NULL, NULL, 1612586299, 1612586299),
('/percakapan-layanan-dosen/bulk-delete', 2, NULL, NULL, NULL, 1612586298, 1612586298),
('/percakapan-layanan-dosen/chat-dosen', 2, NULL, NULL, NULL, 1612601310, 1612601310),
('/percakapan-layanan-dosen/create', 2, NULL, NULL, NULL, 1612586298, 1612586298),
('/percakapan-layanan-dosen/delete', 2, NULL, NULL, NULL, 1612586298, 1612586298),
('/percakapan-layanan-dosen/index', 2, NULL, NULL, NULL, 1612586298, 1612586298),
('/percakapan-layanan-dosen/update', 2, NULL, NULL, NULL, 1612586298, 1612586298),
('/percakapan-layanan-dosen/view', 2, NULL, NULL, NULL, 1612586298, 1612586298),
('/percakapan-layanan-prodi/*', 2, NULL, NULL, NULL, 1613654454, 1613654454),
('/percakapan-layanan-prodi/bulk-delete', 2, NULL, NULL, NULL, 1613654454, 1613654454),
('/percakapan-layanan-prodi/chat-prodi', 2, NULL, NULL, NULL, 1613656837, 1613656837),
('/percakapan-layanan-prodi/create', 2, NULL, NULL, NULL, 1613654453, 1613654453),
('/percakapan-layanan-prodi/delete', 2, NULL, NULL, NULL, 1613654454, 1613654454),
('/percakapan-layanan-prodi/index', 2, NULL, NULL, NULL, 1613654450, 1613654450),
('/percakapan-layanan-prodi/update', 2, NULL, NULL, NULL, 1613654453, 1613654453),
('/percakapan-layanan-prodi/view', 2, NULL, NULL, NULL, 1613654453, 1613654453),
('/percakapan/*', 2, NULL, NULL, NULL, 1602911260, 1602911260),
('/percakapan/bulk-delete', 2, NULL, NULL, NULL, 1602911260, 1602911260),
('/percakapan/create', 2, NULL, NULL, NULL, 1602911260, 1602911260),
('/percakapan/delete', 2, NULL, NULL, NULL, 1602911260, 1602911260),
('/percakapan/index', 2, NULL, NULL, NULL, 1602911259, 1602911259),
('/percakapan/update', 2, NULL, NULL, NULL, 1602911260, 1602911260),
('/percakapan/view', 2, NULL, NULL, NULL, 1602911260, 1602911260),
('/program-studi/*', 2, NULL, NULL, NULL, 1602860548, 1602860548),
('/program-studi/bulk-delete', 2, NULL, NULL, NULL, 1602860548, 1602860548),
('/program-studi/create', 2, NULL, NULL, NULL, 1602860548, 1602860548),
('/program-studi/delete', 2, NULL, NULL, NULL, 1602860548, 1602860548),
('/program-studi/index', 2, NULL, NULL, NULL, 1602860547, 1602860547),
('/program-studi/update', 2, NULL, NULL, NULL, 1602860548, 1602860548),
('/program-studi/view', 2, NULL, NULL, NULL, 1602860548, 1602860548),
('/rest/*', 2, NULL, NULL, NULL, 1579666586, 1579666586),
('/site/*', 2, NULL, NULL, NULL, 1574934094, 1574934094),
('/site/about', 2, NULL, NULL, NULL, 1574934094, 1574934094),
('/site/captcha', 2, NULL, NULL, NULL, 1574934094, 1574934094),
('/site/chat-admin', 2, NULL, NULL, NULL, 1603596707, 1603596707),
('/site/chat-dosen', 2, NULL, NULL, NULL, 1612584457, 1612584457),
('/site/contact', 2, NULL, NULL, NULL, 1574934094, 1574934094),
('/site/error', 2, NULL, NULL, NULL, 1574934094, 1574934094),
('/site/ganti-password', 2, NULL, NULL, NULL, 1604116560, 1604116560),
('/site/index', 2, NULL, NULL, NULL, 1574934094, 1574934094),
('/site/login', 2, NULL, NULL, NULL, 1574934094, 1574934094),
('/site/logout', 2, NULL, NULL, NULL, 1574934094, 1574934094),
('/tidak-lulus-bimbingan/index', 2, NULL, NULL, NULL, 1613491090, 1613491090),
('/tidak-lulus-bimbingan/view', 2, NULL, NULL, NULL, 1614409762, 1614409762),
('/user/*', 2, NULL, NULL, NULL, 1580350722, 1580350722),
('/user/activate', 2, NULL, NULL, NULL, 1580350722, 1580350722),
('/user/change-password', 2, NULL, NULL, NULL, 1580350722, 1580350722),
('/user/delete', 2, NULL, NULL, NULL, 1580350722, 1580350722),
('/user/index', 2, NULL, NULL, NULL, 1580350722, 1580350722),
('/user/login', 2, NULL, NULL, NULL, 1580350722, 1580350722),
('/user/logout', 2, NULL, NULL, NULL, 1580350722, 1580350722),
('/user/request-password-reset', 2, NULL, NULL, NULL, 1580350722, 1580350722),
('/user/reset-password', 2, NULL, NULL, NULL, 1580350722, 1580350722),
('/user/signup', 2, NULL, NULL, NULL, 1580350722, 1580350722),
('/user/update', 2, NULL, NULL, NULL, 1580350722, 1580350722),
('/user/uploadPhoto', 2, NULL, NULL, NULL, 1609391301, 1609391301),
('/user/view', 2, NULL, NULL, NULL, 1580350722, 1580350722),
('/userpejabat/*', 2, NULL, NULL, NULL, 1613894374, 1613894374),
('/userpejabat/bulk-delete', 2, NULL, NULL, NULL, 1613894374, 1613894374),
('/userpejabat/create', 2, NULL, NULL, NULL, 1613894374, 1613894374),
('/userpejabat/delete', 2, NULL, NULL, NULL, 1613894374, 1613894374),
('/userpejabat/index', 2, NULL, NULL, NULL, 1613894374, 1613894374),
('/userpejabat/ubah-profil', 2, NULL, NULL, NULL, 1613899130, 1613899130),
('/userpejabat/update', 2, NULL, NULL, NULL, 1613894374, 1613894374),
('/userpejabat/view', 2, NULL, NULL, NULL, 1613894374, 1613894374),
('Admin Layanan', 1, NULL, NULL, NULL, 1602901875, 1602901875),
('Admin Mahasiswa', 1, NULL, NULL, NULL, 1614327993, 1614327993),
('Developer', 1, NULL, NULL, NULL, 1574934108, 1574934108),
('Dosen', 1, NULL, NULL, NULL, 1609386711, 1609386711),
('Mahasiswa', 1, NULL, NULL, NULL, 1602901856, 1602901856),
('Program Studi', 1, NULL, NULL, NULL, 1612581766, 1612581766);

-- --------------------------------------------------------

--
-- Table structure for table `auth_item_child`
--

DROP TABLE IF EXISTS `auth_item_child`;
CREATE TABLE `auth_item_child` (
  `parent` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_item_child`
--

INSERT INTO `auth_item_child` (`parent`, `child`) VALUES
('Developer', '/*'),
('Developer', '/admin-layanan/*'),
('Developer', '/admin-layanan/bulk-delete'),
('Developer', '/admin-layanan/create'),
('Developer', '/admin-layanan/delete'),
('Developer', '/admin-layanan/index'),
('Admin Layanan', '/admin-layanan/ubah-profil'),
('Developer', '/admin-layanan/ubah-profil'),
('Developer', '/admin-layanan/update'),
('Admin Layanan', '/admin-layanan/view'),
('Developer', '/admin-layanan/view'),
('Developer', '/admin-mahasiswa/*'),
('Developer', '/admin-mahasiswa/bulk-delete'),
('Developer', '/admin-mahasiswa/create'),
('Developer', '/admin-mahasiswa/delete'),
('Developer', '/admin-mahasiswa/index'),
('Admin Mahasiswa', '/admin-mahasiswa/ubah-profil'),
('Developer', '/admin-mahasiswa/update'),
('Admin Mahasiswa', '/admin-mahasiswa/view'),
('Developer', '/admin-mahasiswa/view'),
('Developer', '/admin-prodi/*'),
('Developer', '/admin-prodi/bulk-delete'),
('Developer', '/admin-prodi/create'),
('Developer', '/admin-prodi/delete'),
('Developer', '/admin-prodi/index'),
('Developer', '/admin-prodi/ubah-profil'),
('Program Studi', '/admin-prodi/ubah-profil'),
('Developer', '/admin-prodi/update'),
('Developer', '/admin-prodi/view'),
('Program Studi', '/admin-prodi/view'),
('Developer', '/admin/*'),
('Developer', '/admin/assignment/*'),
('Developer', '/admin/assignment/assign'),
('Developer', '/admin/assignment/index'),
('Developer', '/admin/assignment/revoke'),
('Developer', '/admin/assignment/view'),
('Developer', '/admin/default/*'),
('Developer', '/admin/default/index'),
('Developer', '/admin/menu/*'),
('Developer', '/admin/menu/create'),
('Developer', '/admin/menu/delete'),
('Developer', '/admin/menu/index'),
('Developer', '/admin/menu/update'),
('Developer', '/admin/menu/view'),
('Developer', '/admin/permission/*'),
('Developer', '/admin/permission/assign'),
('Developer', '/admin/permission/create'),
('Developer', '/admin/permission/delete'),
('Developer', '/admin/permission/index'),
('Developer', '/admin/permission/remove'),
('Developer', '/admin/permission/update'),
('Developer', '/admin/permission/view'),
('Developer', '/admin/role/*'),
('Developer', '/admin/role/assign'),
('Developer', '/admin/role/create'),
('Developer', '/admin/role/delete'),
('Developer', '/admin/role/index'),
('Developer', '/admin/role/remove'),
('Developer', '/admin/role/update'),
('Developer', '/admin/role/view'),
('Developer', '/admin/route/*'),
('Developer', '/admin/route/assign'),
('Developer', '/admin/route/create'),
('Developer', '/admin/route/index'),
('Developer', '/admin/route/refresh'),
('Developer', '/admin/route/remove'),
('Developer', '/admin/rule/*'),
('Developer', '/admin/rule/create'),
('Developer', '/admin/rule/delete'),
('Developer', '/admin/rule/index'),
('Developer', '/admin/rule/update'),
('Developer', '/admin/rule/view'),
('Developer', '/admin/user/*'),
('Developer', '/admin/user/activate'),
('Developer', '/admin/user/change-password'),
('Developer', '/admin/user/delete'),
('Developer', '/admin/user/index'),
('Developer', '/admin/user/login'),
('Developer', '/admin/user/logout'),
('Developer', '/admin/user/request-password-reset'),
('Developer', '/admin/user/reset-password'),
('Developer', '/admin/user/signup'),
('Developer', '/admin/user/view'),
('Developer', '/bimbingan/*'),
('Developer', '/bimbingan/bulk-delete'),
('Developer', '/bimbingan/create'),
('Mahasiswa', '/bimbingan/create'),
('Admin Layanan', '/bimbingan/data-ikut-ujian'),
('Developer', '/bimbingan/data-ikut-ujian'),
('Dosen', '/bimbingan/data-ikut-ujian'),
('Admin Layanan', '/bimbingan/data-kurang-bimbingan'),
('Developer', '/bimbingan/data-kurang-bimbingan'),
('Dosen', '/bimbingan/data-kurang-bimbingan'),
('Developer', '/bimbingan/data-mahasiswa'),
('Dosen', '/bimbingan/data-mahasiswa'),
('Developer', '/bimbingan/data-setuju'),
('Dosen', '/bimbingan/data-setuju'),
('Developer', '/bimbingan/data-tolak'),
('Developer', '/bimbingan/delete'),
('Developer', '/bimbingan/index'),
('Dosen', '/bimbingan/index'),
('Mahasiswa', '/bimbingan/index'),
('Developer', '/bimbingan/setuju'),
('Dosen', '/bimbingan/setuju'),
('Developer', '/bimbingan/update'),
('Dosen', '/bimbingan/update'),
('Developer', '/bimbingan/view'),
('Dosen', '/bimbingan/view'),
('Mahasiswa', '/bimbingan/view'),
('Dosen', '/bimbingan/view-mahasiswa'),
('Developer', '/debug/*'),
('Developer', '/debug/default/*'),
('Developer', '/debug/default/db-explain'),
('Developer', '/debug/default/download-mail'),
('Developer', '/debug/default/index'),
('Developer', '/debug/default/toolbar'),
('Developer', '/debug/default/view'),
('Developer', '/debug/user/*'),
('Developer', '/debug/user/reset-identity'),
('Developer', '/debug/user/set-identity'),
('Developer', '/dosen/*'),
('Developer', '/dosen/bulk-delete'),
('Developer', '/dosen/create'),
('Developer', '/dosen/delete'),
('Developer', '/dosen/index'),
('Developer', '/dosen/ubah-profil'),
('Dosen', '/dosen/ubah-profil'),
('Developer', '/dosen/update'),
('Developer', '/dosen/view'),
('Dosen', '/dosen/view'),
('Developer', '/gii/*'),
('Developer', '/gii/default/*'),
('Developer', '/gii/default/action'),
('Developer', '/gii/default/diff'),
('Developer', '/gii/default/index'),
('Developer', '/gii/default/preview'),
('Developer', '/gii/default/view'),
('Developer', '/golongan/*'),
('Developer', '/golongan/bulk-delete'),
('Developer', '/golongan/create'),
('Developer', '/golongan/delete'),
('Developer', '/golongan/index'),
('Developer', '/golongan/update'),
('Developer', '/golongan/view'),
('Developer', '/gridview/*'),
('Developer', '/gridview/export/*'),
('Developer', '/gridview/export/download'),
('Developer', '/jabatan/*'),
('Developer', '/jabatan/bulk-delete'),
('Developer', '/jabatan/create'),
('Developer', '/jabatan/delete'),
('Developer', '/jabatan/index'),
('Developer', '/jabatan/update'),
('Developer', '/jabatan/view'),
('Admin Layanan', '/lulus-bimbingan/index'),
('Admin Mahasiswa', '/lulus-bimbingan/index'),
('Developer', '/lulus-bimbingan/index'),
('Dosen', '/lulus-bimbingan/index'),
('Admin Layanan', '/lulus-bimbingan/proses-cek-bimbingan'),
('Admin Layanan', '/lulus-bimbingan/view'),
('Admin Mahasiswa', '/lulus-bimbingan/view'),
('Dosen', '/lulus-bimbingan/view'),
('Developer', '/mahasiswa/*'),
('Developer', '/mahasiswa/bulk-delete'),
('Developer', '/mahasiswa/create'),
('Mahasiswa', '/mahasiswa/create'),
('Developer', '/mahasiswa/delete'),
('Developer', '/mahasiswa/index'),
('Developer', '/mahasiswa/ubah-profil'),
('Mahasiswa', '/mahasiswa/ubah-profil'),
('Developer', '/mahasiswa/update'),
('Mahasiswa', '/mahasiswa/update'),
('Developer', '/mahasiswa/view'),
('Mahasiswa', '/mahasiswa/view'),
('Admin Layanan', '/percakapan-layanan-adma/chat-adma'),
('Admin Layanan', '/percakapan-layanan-adma/index'),
('Admin Mahasiswa', '/percakapan-layanan-adma/index'),
('Developer', '/percakapan-layanan-dosen/*'),
('Developer', '/percakapan-layanan-dosen/bulk-delete'),
('Admin Layanan', '/percakapan-layanan-dosen/chat-dosen'),
('Developer', '/percakapan-layanan-dosen/chat-dosen'),
('Developer', '/percakapan-layanan-dosen/create'),
('Developer', '/percakapan-layanan-dosen/delete'),
('Admin Layanan', '/percakapan-layanan-dosen/index'),
('Developer', '/percakapan-layanan-dosen/index'),
('Dosen', '/percakapan-layanan-dosen/index'),
('Developer', '/percakapan-layanan-dosen/update'),
('Developer', '/percakapan-layanan-dosen/view'),
('Developer', '/percakapan-layanan-prodi/*'),
('Developer', '/percakapan-layanan-prodi/bulk-delete'),
('Admin Layanan', '/percakapan-layanan-prodi/chat-prodi'),
('Developer', '/percakapan-layanan-prodi/chat-prodi'),
('Developer', '/percakapan-layanan-prodi/create'),
('Developer', '/percakapan-layanan-prodi/delete'),
('Admin Layanan', '/percakapan-layanan-prodi/index'),
('Developer', '/percakapan-layanan-prodi/index'),
('Program Studi', '/percakapan-layanan-prodi/index'),
('Developer', '/percakapan-layanan-prodi/update'),
('Developer', '/percakapan-layanan-prodi/view'),
('Admin Layanan', '/percakapan/*'),
('Developer', '/percakapan/*'),
('Mahasiswa', '/percakapan/*'),
('Admin Layanan', '/percakapan/bulk-delete'),
('Developer', '/percakapan/bulk-delete'),
('Mahasiswa', '/percakapan/bulk-delete'),
('Admin Layanan', '/percakapan/create'),
('Developer', '/percakapan/create'),
('Mahasiswa', '/percakapan/create'),
('Admin Layanan', '/percakapan/delete'),
('Developer', '/percakapan/delete'),
('Mahasiswa', '/percakapan/delete'),
('Admin Layanan', '/percakapan/index'),
('Developer', '/percakapan/index'),
('Mahasiswa', '/percakapan/index'),
('Admin Layanan', '/percakapan/update'),
('Developer', '/percakapan/update'),
('Mahasiswa', '/percakapan/update'),
('Admin Layanan', '/percakapan/view'),
('Developer', '/percakapan/view'),
('Mahasiswa', '/percakapan/view'),
('Developer', '/program-studi/*'),
('Developer', '/program-studi/bulk-delete'),
('Developer', '/program-studi/create'),
('Developer', '/program-studi/delete'),
('Developer', '/program-studi/index'),
('Developer', '/program-studi/update'),
('Developer', '/program-studi/view'),
('Developer', '/rest/*'),
('Developer', '/site/*'),
('Developer', '/site/about'),
('Developer', '/site/captcha'),
('Admin Layanan', '/site/chat-admin'),
('Developer', '/site/chat-admin'),
('Admin Layanan', '/site/chat-dosen'),
('Developer', '/site/chat-dosen'),
('Dosen', '/site/chat-dosen'),
('Developer', '/site/contact'),
('Developer', '/site/error'),
('Admin Layanan', '/site/ganti-password'),
('Developer', '/site/ganti-password'),
('Mahasiswa', '/site/ganti-password'),
('Program Studi', '/site/ganti-password'),
('Admin Layanan', '/site/index'),
('Admin Mahasiswa', '/site/index'),
('Developer', '/site/index'),
('Dosen', '/site/index'),
('Mahasiswa', '/site/index'),
('Program Studi', '/site/index'),
('Developer', '/site/login'),
('Dosen', '/site/login'),
('Admin Layanan', '/site/logout'),
('Admin Mahasiswa', '/site/logout'),
('Developer', '/site/logout'),
('Dosen', '/site/logout'),
('Mahasiswa', '/site/logout'),
('Program Studi', '/site/logout'),
('Admin Layanan', '/tidak-lulus-bimbingan/index'),
('Admin Mahasiswa', '/tidak-lulus-bimbingan/index'),
('Developer', '/tidak-lulus-bimbingan/index'),
('Dosen', '/tidak-lulus-bimbingan/index'),
('Admin Layanan', '/tidak-lulus-bimbingan/view'),
('Admin Mahasiswa', '/tidak-lulus-bimbingan/view'),
('Dosen', '/tidak-lulus-bimbingan/view'),
('Developer', '/user/*'),
('Developer', '/user/activate'),
('Admin Layanan', '/user/change-password'),
('Admin Mahasiswa', '/user/change-password'),
('Developer', '/user/change-password'),
('Developer', '/user/delete'),
('Developer', '/user/index'),
('Developer', '/user/login'),
('Developer', '/user/logout'),
('Developer', '/user/request-password-reset'),
('Developer', '/user/reset-password'),
('Developer', '/user/signup'),
('Developer', '/user/update'),
('Admin Layanan', '/user/uploadPhoto'),
('Admin Mahasiswa', '/user/uploadPhoto'),
('Developer', '/user/uploadPhoto'),
('Dosen', '/user/uploadPhoto'),
('Mahasiswa', '/user/uploadPhoto'),
('Program Studi', '/user/uploadPhoto'),
('Developer', '/user/view'),
('Developer', '/userpejabat/*'),
('Developer', '/userpejabat/bulk-delete'),
('Developer', '/userpejabat/create'),
('Developer', '/userpejabat/delete'),
('Developer', '/userpejabat/index'),
('Developer', '/userpejabat/ubah-profil'),
('Developer', '/userpejabat/update'),
('Developer', '/userpejabat/view'),
('Developer', 'Admin Layanan'),
('Developer', 'Dosen'),
('Developer', 'Mahasiswa'),
('Developer', 'Program Studi');

-- --------------------------------------------------------

--
-- Table structure for table `auth_rule`
--

DROP TABLE IF EXISTS `auth_rule`;
CREATE TABLE `auth_rule` (
  `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `bimbingan`
--

DROP TABLE IF EXISTS `bimbingan`;
CREATE TABLE `bimbingan` (
  `id` int(11) NOT NULL,
  `id_dosen` int(11) NOT NULL,
  `id_mahasiswa` int(11) NOT NULL,
  `file` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `topik_bimbingan` varchar(200) DEFAULT NULL,
  `komentar` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `status` int(1) DEFAULT NULL,
  `tanggal` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bimbingan`
--

INSERT INTO `bimbingan` (`id`, `id_dosen`, `id_mahasiswa`, `file`, `topik_bimbingan`, `komentar`, `status`) VALUES
(2, 3, 8, 'design rumah.docx', NULL, 'Belum jelas juga', 0),
(3, 3, 8, 'littlepony.doc.docx', NULL, NULL, 1),
(4, 3, 6, 'kop.docx', NULL, NULL, 1),
(5, 3, 8, 'data pemilik kendaraan.xlsx', NULL, NULL, NULL),
(6, 4, 9, 'invoice.docx', NULL, NULL, 1),
(7, 4, 9, 'design rumah.docx', 'Analis Pariwisata', 'Bimbingan di setujui', 1),
(8, 4, 9, 'CV Tommy Suindra.docx', 'tesss', NULL, NULL),
(9, 4, 9, NULL, 'sfafasfsa', NULL, NULL),
(10, 3, 6, NULL, 'tesss', NULL, NULL),
(11, 5, 9, NULL, 'Pak Robby', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `dosen`
--

DROP TABLE IF EXISTS `dosen`;
CREATE TABLE `dosen` (
  `id` int(11) NOT NULL,
  `nip` varchar(25) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `jenis_kelamin` varchar(20) NOT NULL,
  `id_jabatan` int(11) DEFAULT NULL,
  `id_golongan` int(11) DEFAULT NULL,
  `foto` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dosen`
--

INSERT INTO `dosen` (`id`, `nip`, `nama`, `jenis_kelamin`, `id_jabatan`, `id_golongan`, `foto`) VALUES
(3, '2020001', 'Burhanuddin', 'L', 1, 1, '../media/foto/5ff173d8566a5.png'),
(4, '2020002', 'Dini', 'P', 1, 2, NULL),
(5, '20210003', 'Robby', 'L', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `dosen_tutor`
--

DROP TABLE IF EXISTS `dosen_tutor`;
CREATE TABLE `dosen_tutor` (
  `id` int(11) NOT NULL,
  `id_dosen` int(11) NOT NULL,
  `id_mahasiswa` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dosen_tutor`
--

INSERT INTO `dosen_tutor` (`id`, `id_dosen`, `id_mahasiswa`) VALUES
(4, 5, 9);

-- --------------------------------------------------------

--
-- Table structure for table `golongan`
--

DROP TABLE IF EXISTS `golongan`;
CREATE TABLE `golongan` (
  `id` int(11) NOT NULL,
  `nama_golongan` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `golongan`
--

INSERT INTO `golongan` (`id`, `nama_golongan`) VALUES
(1, 'II/a'),
(2, 'III/a');

-- --------------------------------------------------------

--
-- Table structure for table `jabatan`
--

DROP TABLE IF EXISTS `jabatan`;
CREATE TABLE `jabatan` (
  `id` int(11) NOT NULL,
  `nama_jabatan` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jabatan`
--

INSERT INTO `jabatan` (`id`, `nama_jabatan`) VALUES
(1, 'Kanit Konseling'),
(2, 'Sekretaris Konseling');

-- --------------------------------------------------------

--
-- Table structure for table `lulus_bimbingan`
--

DROP TABLE IF EXISTS `lulus_bimbingan`;
CREATE TABLE `lulus_bimbingan` (
  `id` int(11) NOT NULL,
  `id_mahasiswa` int(11) NOT NULL,
  `semester` int(3) NOT NULL,
  `jumlah_bimbingan` int(5) NOT NULL,
  `tanggal` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `lulus_bimbingan`
--

INSERT INTO `lulus_bimbingan` (`id`, `id_mahasiswa`, `semester`, `jumlah_bimbingan`) VALUES
(4, 9, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `mahasiswa`
--

DROP TABLE IF EXISTS `mahasiswa`;
CREATE TABLE `mahasiswa` (
  `id` int(11) NOT NULL,
  `idprodi` int(11) NOT NULL,
  `nim` varchar(25) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `kelas` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `semester` int(2) NOT NULL,
  `tempat_lahir` varchar(255) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `alamat` text NOT NULL,
  `foto` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mahasiswa`
--

INSERT INTO `mahasiswa` (`id`, `idprodi`, `nim`, `nama`, `kelas`, `semester`, `tempat_lahir`, `tanggal_lahir`, `alamat`, `foto`) VALUES
(6, 1, '123456', 'testing', '', 0, 'Medan', '2020-10-17', 'JLn Medan', ''),
(7, 2, '123455', 'coba coba', '', 0, 'Medan', '2020-10-21', 'jalan medan', NULL),
(8, 3, '123456789', 'Johan Surasa', 'A2', 8, 'Medan', '2020-02-27', 'Jalan', '../media/foto/5fed5d9d2fc94.jpg'),
(9, 4, '2100001', 'Bastanta', 'MPPP-1', 1, 'Medan', '2021-01-19', 'Jalan Marelan', '../media/foto/6006d76d4c0cc.png');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `parent` int(11) DEFAULT NULL,
  `route` varchar(255) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `data` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `name`, `parent`, `route`, `order`, `data`) VALUES
(39, 'Mahasiswa', NULL, '/mahasiswa/index', 3, 'users'),
(40, 'Program Studi', NULL, '/program-studi/index', 7, 'sitemap'),
(41, 'Jabatan', NULL, '/jabatan/index', 6, 'sort-amount-desc'),
(42, 'Admin Layanan', NULL, '/admin-layanan/index', 4, 'user-secret'),
(43, 'Dosen', NULL, '/dosen/index', 2, 'user'),
(44, 'Golongan', NULL, '/golongan/index', 7, 'tasks'),
(45, 'Bimbingan', NULL, NULL, 1, 'book'),
(46, 'Tutor', 45, '/bimbingan/index', 1, 'send '),
(47, 'Setuju', 45, '/bimbingan/data-setuju', 2, 'check-square'),
(48, 'Tolak', 45, '/bimbingan/data-tolak', 3, 'times-circle'),
(49, 'Lulus Bimbingan', 45, '/lulus-bimbingan/index', 5, 'street-view'),
(50, 'Kurang Bimbingan', 45, '/tidak-lulus-bimbingan/index', 4, 'outdent'),
(52, 'Layanan Konseling', NULL, NULL, 1, 'fax'),
(53, 'Dosen', 52, '/percakapan-layanan-dosen/index', NULL, 'user'),
(54, 'Admin Prodi', NULL, '/admin-prodi/index', 5, 'user-md'),
(55, 'Program Studi', 52, '/percakapan-layanan-prodi/index', NULL, 'user-md'),
(56, 'Mahasiswa', 52, '/percakapan/index', NULL, 'users'),
(57, 'Developer', NULL, NULL, NULL, 'gears'),
(58, 'User', 57, '/userpejabat/index', NULL, 'user-plus'),
(59, 'Admin Mahasiswa', NULL, '/admin-mahasiswa/index', 6, 'graduation-cap'),
(60, 'Admin Mahasiswa', 52, '/percakapan-layanan-adma/index', NULL, 'graduation-cap'),
(61, 'Proses Bimbingan', 45, '/lulus-bimbingan/proses-cek-bimbingan', NULL, 'cogs');

-- --------------------------------------------------------

--
-- Table structure for table `percakapan`
--

DROP TABLE IF EXISTS `percakapan`;
CREATE TABLE `percakapan` (
  `id` bigint(20) NOT NULL,
  `idmahasiswa` int(11) DEFAULT NULL,
  `idadminlayanan` int(11) DEFAULT NULL,
  `idtujuan` int(11) DEFAULT NULL,
  `isi` text,
  `dokumen` varchar(255) DEFAULT NULL,
  `tanggal` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status_baca` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `percakapan`
--

INSERT INTO `percakapan` (`id`, `idmahasiswa`, `idadminlayanan`, `idtujuan`, `isi`, `dokumen`, `status_baca`) VALUES
(16, 9, NULL, NULL, 'halo pak', NULL, 1),
(17, 9, NULL, NULL, 'ini file nya ya pak', 'kop.docx', 1),
(18, 9, 7, NULL, 'baik diterima\r\nsaya kirim kembali laporannya', 'design rumah.docx', 1),
(19, 9, NULL, NULL, 'haiiii', NULL, 1),
(20, 9, NULL, NULL, 'ini ya', 'invoice.docx', 1),
(21, 9, 7, NULL, 'ok', NULL, 1),
(22, 6, NULL, NULL, 'haloooo', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `percakapan_layanan_adma`
--

DROP TABLE IF EXISTS `percakapan_layanan_adma`;
CREATE TABLE `percakapan_layanan_adma` (
  `id` bigint(20) NOT NULL,
  `idadminmahasiswa` int(11) DEFAULT NULL,
  `idadminlayanan` int(11) DEFAULT NULL,
  `idtujuan` int(11) DEFAULT NULL,
  `isi` text,
  `dokumen` varchar(255) DEFAULT NULL,
  `tanggal` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status_baca` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `percakapan_layanan_adma`
--

INSERT INTO `percakapan_layanan_adma` (`id`, `idadminmahasiswa`, `idadminlayanan`, `idtujuan`, `isi`, `dokumen`, `status_baca`) VALUES
(29, 4, NULL, NULL, 'haloo', NULL, 1),
(30, 4, NULL, NULL, 'ini filenya ya', 'invoice.docx', 1),
(31, 4, 7, NULL, 'ok saya terima', NULL, 1),
(32, 4, 7, NULL, 'ini file balasannya', 'CONTOH.docx', 1),
(33, 5, NULL, NULL, 'tesssss', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `percakapan_layanan_dosen`
--

DROP TABLE IF EXISTS `percakapan_layanan_dosen`;
CREATE TABLE `percakapan_layanan_dosen` (
  `id` bigint(20) NOT NULL,
  `iddosen` int(11) DEFAULT NULL,
  `idadminlayanan` int(11) DEFAULT NULL,
  `idtujuan` int(11) DEFAULT NULL,
  `isi` text,
  `dokumen` varchar(255) DEFAULT NULL,
  `tanggal` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status_baca` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `percakapan_layanan_dosen`
--

INSERT INTO `percakapan_layanan_dosen` (`id`, `iddosen`, `idadminlayanan`, `idtujuan`, `isi`, `dokumen`, `status_baca`) VALUES
(1, 6, NULL, NULL, 'tessss', NULL, 1),
(2, 6, NULL, NULL, 'tessss4444', NULL, 1),
(3, 6, NULL, NULL, 'halooo', NULL, 1),
(4, 6, NULL, NULL, 'halooo', NULL, 1),
(5, 7, NULL, NULL, 'Selama Siang', NULL, 1),
(6, 7, NULL, NULL, 'Apa Kabar', NULL, 1),
(7, 6, 7, NULL, 'ok mantap', NULL, 1),
(8, 6, NULL, NULL, 'sama2 ya', NULL, 1),
(9, 7, 7, NULL, 'Baikk..', NULL, 1),
(10, 6, NULL, NULL, 'tes tesss', NULL, 1),
(11, 6, 7, NULL, 'iya ada apa..?', NULL, 1),
(12, 7, NULL, NULL, 'halollll', NULL, 1),
(13, 7, 7, NULL, 'ok mantap', NULL, 1),
(14, 8, NULL, NULL, 'Haiii', NULL, 1),
(15, 8, 7, NULL, 'selamat pagi', NULL, 1),
(16, 5, NULL, NULL, 'halooooooo', NULL, 1),
(17, 5, NULL, NULL, 'apa kabar', NULL, 1),
(19, 5, NULL, NULL, 'hihihii', NULL, 1),
(20, 5, NULL, NULL, 'ini filenya', 'analisis KBI (allya fadillah) rev1.docx', 1),
(21, 5, 7, NULL, 'ok sudah diterima', NULL, 1),
(22, 5, 7, NULL, 'ini file nya ya', 'littlepony.doc.docx', 1);

-- --------------------------------------------------------

--
-- Table structure for table `percakapan_layanan_prodi`
--

DROP TABLE IF EXISTS `percakapan_layanan_prodi`;
CREATE TABLE `percakapan_layanan_prodi` (
  `id` bigint(20) NOT NULL,
  `idadminprodi` int(11) DEFAULT NULL,
  `idadminlayanan` int(11) DEFAULT NULL,
  `idtujuan` int(11) DEFAULT NULL,
  `isi` text,
  `dokumen` varchar(255) DEFAULT NULL,
  `tanggal` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status_baca` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `percakapan_layanan_prodi`
--

INSERT INTO `percakapan_layanan_prodi` (`id`, `idadminprodi`, `idadminlayanan`, `idtujuan`, `isi`, `dokumen`, `status_baca`) VALUES
(24, 2, NULL, NULL, 'haloooo', NULL, 1),
(25, 2, NULL, NULL, 'sedang apa?', NULL, 1),
(26, 2, NULL, NULL, 'ini ya filenya', 'analisis KBI (allya fadillah) rev1.docx', 1),
(27, 2, 7, NULL, 'ok sudah diterima', NULL, 1),
(28, 2, 7, NULL, 'ini file balasan', 'design rumah.docx', 1);

-- --------------------------------------------------------

--
-- Table structure for table `program_studi`
--

DROP TABLE IF EXISTS `program_studi`;
CREATE TABLE `program_studi` (
  `id` int(11) NOT NULL,
  `nama_prodi` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `program_studi`
--

INSERT INTO `program_studi` (`id`, `nama_prodi`) VALUES
(1, 'PPH'),
(2, 'MTH'),
(3, 'MTB'),
(4, 'MPPP'),
(5, 'MUP'),
(6, 'MDK');

-- --------------------------------------------------------

--
-- Table structure for table `tidak_lulus_bimbingan`
--

DROP TABLE IF EXISTS `tidak_lulus_bimbingan`;
CREATE TABLE `tidak_lulus_bimbingan` (
  `id` int(11) NOT NULL,
  `id_mahasiswa` int(11) NOT NULL,
  `semester` int(3) NOT NULL,
  `jumlah_bimbingan` int(5) NOT NULL,
  `tanggal` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tidak_lulus_bimbingan`
--

INSERT INTO `tidak_lulus_bimbingan` (`id`, `id_mahasiswa`, `semester`, `jumlah_bimbingan`) VALUES
(22, 6, 0, 1),
(23, 8, 8, 1),
(24, 9, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `role` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'jabatan',
  `id_user` int(11) DEFAULT NULL,
  `foto` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`, `role`, `id_user`, `foto`) VALUES
(1, 'developer', 'M9CtXLM7gA_ufZKuurqNsXEXBWwxmyP1', '$2y$13$EwSvFWEKRDT3TaJ9rqw86OqS3vFZxQJLjAGXxModuKNaBxumDpYv6', NULL, 'developer@gmail.com', 10, 1602859977, 1602859977, 'Developer', NULL, NULL),
(18, 'robby', 'N_P0vv1HBNnaXB2xQ2Un_BQEp-WptW6g', '$2y$13$.1PtVfb6wmu6MiKkFde8BuJGsi5k86iaq/6pP7QAcA4cHufD91pjO', NULL, NULL, 10, 1602910809, 1602910809, 'Admin Layanan', 6, NULL),
(19, '123456', 'f4QShsqdL-6Q9Bz8JC1F_qR5OZqgzI2a', '$2y$13$SuR/Q6XlckSf5PIMoMuB5O5s6M88H5ogNraH0iNrSQjHmrQCKkjRa', NULL, NULL, 10, 1602910941, 1602910941, 'Mahasiswa', 6, NULL),
(20, '345678', '56iZO7OZKcQNwkPigKzNh9hIkgbrO9I2', '$2y$13$AhF5eGR5orqg1ZbBIa3LHuOlWP48J65qRCiaAV23/vY1HSzXbe.Dy', NULL, NULL, 10, 1603118085, 1603118085, 'Admin Layanan', 7, NULL),
(21, '123455', 'gxDKIGeXoefuUlYScPbxDXuJjX91PkNk', '$2y$13$eUkI7eENzq36B6cP0bvw/.x.PMLd9Cu.uxcte.UZQm2VLNGt5y3y2', NULL, NULL, 10, 1603291651, 1604116863, 'Mahasiswa', 7, NULL),
(22, '123456789', 'wzOSr2ScNVvwyniBZXDdDtaC5AUX_Y8r', '$2y$13$RBfkCkCIT/AEXsNz35h0b.OTNru8J3JQQRT/KufB4NeT4TiYxaZZC', NULL, NULL, 10, 1609160870, 1609160870, 'Mahasiswa', 8, NULL),
(24, '2020001', 'tkelRgZNICpMUPd4IOmtxe6rZWi3geA_', '$2y$13$aM4ELcYQUCQ0h76z8qbtHuJUFUgJ8mXfVBTsyagzejR1.YGZGJB6O', NULL, NULL, 10, 1609389237, 1609389237, 'Dosen', 3, NULL),
(25, '2020002', '6zM5p8LcESt4HaynRWfysImE-CAsZb4O', '$2y$13$qlAKBOSZ0qPrV5D6koqW3ObZqhvvjOLWadvTBeFhNR5NmScDPhsja', NULL, NULL, 10, 1609685377, 1609685377, 'Dosen', 4, NULL),
(26, '2100001', 'iSlSzEeGPBNuEFOt2gMMD2KjLnLFaxzB', '$2y$13$8lGQ28KPBFfFLatlJ51rZO5EglD6D0P4tYDWzEalZFVIMalbr1UqG', NULL, NULL, 10, 1611061006, 1611061006, 'Mahasiswa', 9, NULL),
(27, '20210003', 'qtFMdXZo473rkPXXAFhY6nNO9cCfFaGl', '$2y$13$1irgrRTysEVk2MQSfrkrDeL3c8g8N7X75kBXpEs.MPYlccpp4RxyG', NULL, NULL, 10, 1612582501, 1612582501, 'Dosen', 5, NULL),
(28, '21210001', 'KFwIaCrotVmsTh9o6sSh8f2wvceEIxOh', '$2y$13$SzRJ9utZMjzTxlZyoCMS3OL3ALJWA7UoTPQ.F2J4a916pA7uR.rFy', NULL, NULL, 10, 1613641887, 1613641887, 'Program Studi', 2, NULL),
(31, 'admin_khusus', 'tg2ywXFPCJFPge6Eyc1QxTItZJKhp638', '$2y$13$gah8KAhgtdNAujuylVOJ9Oi91Xoe9JslRa6c24wwPjJNAcVg0Fd/K', NULL, 'admin_khusus@gmail.com', 10, 1613898492, 1613900694, 'Developer', NULL, '../media/foto/60322b915b6f0.jpg'),
(32, '65444444', 'VcH2ZQURP8sSq3-3vBhYQ2xcJqFI0Leh', '$2y$13$V4Ij35wsWwiF4Qkvw1t2Ae8JPpFh4BeCz9a7h.SDsMMoV.duF0Anq', NULL, NULL, 10, 1614328680, 1614328680, 'Admin Mahasiswa', 4, NULL),
(33, '999900001', '2NALPgIVNQ5yAXlgieW-3l4DHu0R5U9c', '$2y$13$1c/5PuvG5lWVhzAHGYzGEO1J9XyFpY0Ln70I72Vme3cMrbPOL3XhC', NULL, NULL, 10, 1614407441, 1614407441, 'Admin Mahasiswa', 5, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_layanan`
--
ALTER TABLE `admin_layanan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_admin_layanan_idjabatan` (`idjabatan`);

--
-- Indexes for table `admin_mahasiswa`
--
ALTER TABLE `admin_mahasiswa`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_admin_prodi_idprodi` (`idprodi`),
  ADD KEY `fk_admin_prodi_idjabatan` (`idjabatan`);

--
-- Indexes for table `admin_prodi`
--
ALTER TABLE `admin_prodi`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_admin_prodi_idprodi` (`idprodi`),
  ADD KEY `fk_admin_prodi_idjabatan` (`idjabatan`);

--
-- Indexes for table `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD PRIMARY KEY (`item_name`,`user_id`),
  ADD KEY `idx-auth_assignment-user_id` (`user_id`);

--
-- Indexes for table `auth_item`
--
ALTER TABLE `auth_item`
  ADD PRIMARY KEY (`name`),
  ADD KEY `rule_name` (`rule_name`),
  ADD KEY `idx-auth_item-type` (`type`);

--
-- Indexes for table `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD PRIMARY KEY (`parent`,`child`),
  ADD KEY `child` (`child`);

--
-- Indexes for table `auth_rule`
--
ALTER TABLE `auth_rule`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `bimbingan`
--
ALTER TABLE `bimbingan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_bimbingan_id_dosen` (`id_dosen`),
  ADD KEY `fk_bimbingan_id_mhs` (`id_mahasiswa`);

--
-- Indexes for table `dosen`
--
ALTER TABLE `dosen`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_dosen_id_jabatan` (`id_jabatan`),
  ADD KEY `fk_dosen_id_golongan` (`id_golongan`);

--
-- Indexes for table `dosen_tutor`
--
ALTER TABLE `dosen_tutor`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_dosen_tutor_id_dosen` (`id_dosen`),
  ADD KEY `fk_dosen_tutor_id_mahasiswa` (`id_mahasiswa`);

--
-- Indexes for table `golongan`
--
ALTER TABLE `golongan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jabatan`
--
ALTER TABLE `jabatan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lulus_bimbingan`
--
ALTER TABLE `lulus_bimbingan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_lulus_bimbingan_id_mahasiswa` (`id_mahasiswa`);

--
-- Indexes for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_mahasiswa_idprodi` (`idprodi`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent` (`parent`);

--
-- Indexes for table `percakapan`
--
ALTER TABLE `percakapan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `percakapan_layanan_adma`
--
ALTER TABLE `percakapan_layanan_adma`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `percakapan_layanan_dosen`
--
ALTER TABLE `percakapan_layanan_dosen`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `percakapan_layanan_prodi`
--
ALTER TABLE `percakapan_layanan_prodi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `program_studi`
--
ALTER TABLE `program_studi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tidak_lulus_bimbingan`
--
ALTER TABLE `tidak_lulus_bimbingan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_tidak_lulus_bimbingan_id_mahasiswa` (`id_mahasiswa`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_layanan`
--
ALTER TABLE `admin_layanan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `admin_mahasiswa`
--
ALTER TABLE `admin_mahasiswa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `admin_prodi`
--
ALTER TABLE `admin_prodi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `bimbingan`
--
ALTER TABLE `bimbingan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `dosen`
--
ALTER TABLE `dosen`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `dosen_tutor`
--
ALTER TABLE `dosen_tutor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `golongan`
--
ALTER TABLE `golongan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `jabatan`
--
ALTER TABLE `jabatan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `lulus_bimbingan`
--
ALTER TABLE `lulus_bimbingan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT for table `percakapan`
--
ALTER TABLE `percakapan`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `percakapan_layanan_adma`
--
ALTER TABLE `percakapan_layanan_adma`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `percakapan_layanan_dosen`
--
ALTER TABLE `percakapan_layanan_dosen`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `percakapan_layanan_prodi`
--
ALTER TABLE `percakapan_layanan_prodi`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `program_studi`
--
ALTER TABLE `program_studi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tidak_lulus_bimbingan`
--
ALTER TABLE `tidak_lulus_bimbingan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `admin_layanan`
--
ALTER TABLE `admin_layanan`
  ADD CONSTRAINT `fk_admin_layanan_idjabatan` FOREIGN KEY (`idjabatan`) REFERENCES `jabatan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `admin_mahasiswa`
--
ALTER TABLE `admin_mahasiswa`
  ADD CONSTRAINT `admin_mahasiswa_ibfk_1` FOREIGN KEY (`idjabatan`) REFERENCES `jabatan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `admin_mahasiswa_ibfk_2` FOREIGN KEY (`idprodi`) REFERENCES `program_studi` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `admin_prodi`
--
ALTER TABLE `admin_prodi`
  ADD CONSTRAINT `fk_admin_prodi_idjabatan` FOREIGN KEY (`idjabatan`) REFERENCES `jabatan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_admin_prodi_idprodi` FOREIGN KEY (`idprodi`) REFERENCES `program_studi` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `auth_item`
--
ALTER TABLE `auth_item`
  ADD CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `bimbingan`
--
ALTER TABLE `bimbingan`
  ADD CONSTRAINT `fk_bimbingan_id_dosen` FOREIGN KEY (`id_dosen`) REFERENCES `dosen` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_bimbingan_id_mhs` FOREIGN KEY (`id_mahasiswa`) REFERENCES `mahasiswa` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `dosen`
--
ALTER TABLE `dosen`
  ADD CONSTRAINT `fk_dosen_id_golongan` FOREIGN KEY (`id_golongan`) REFERENCES `golongan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_dosen_id_jabatan` FOREIGN KEY (`id_jabatan`) REFERENCES `jabatan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `dosen_tutor`
--
ALTER TABLE `dosen_tutor`
  ADD CONSTRAINT `fk_dosen_tutor_id_dosen` FOREIGN KEY (`id_dosen`) REFERENCES `dosen` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_dosen_tutor_id_mahasiswa` FOREIGN KEY (`id_mahasiswa`) REFERENCES `mahasiswa` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `lulus_bimbingan`
--
ALTER TABLE `lulus_bimbingan`
  ADD CONSTRAINT `fk_lulus_bimbingan_id_mahasiswa` FOREIGN KEY (`id_mahasiswa`) REFERENCES `mahasiswa` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  ADD CONSTRAINT `fk_mahasiswa_idprodi` FOREIGN KEY (`idprodi`) REFERENCES `program_studi` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `menu`
--
ALTER TABLE `menu`
  ADD CONSTRAINT `menu_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `menu` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `tidak_lulus_bimbingan`
--
ALTER TABLE `tidak_lulus_bimbingan`
  ADD CONSTRAINT `fk_tidak_lulus_bimbingan_id_mahasiswa` FOREIGN KEY (`id_mahasiswa`) REFERENCES `mahasiswa` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

DELIMITER $$
--
-- Events
--
DROP EVENT `eventLulusBimbinganGanjil`$$
CREATE DEFINER=`root`@`localhost` EVENT `eventLulusBimbinganGanjil` ON SCHEDULE EVERY 1 YEAR STARTS '2021-03-19 12:00:00' ENDS '2021-03-19 12:30:00' ON COMPLETION NOT PRESERVE ENABLE DO insert into lulus_bimbingan (id_mahasiswa, semester, jumlah_bimbingan)
select 
	distinct
	mh.id,
	mh.semester,
	(
                    select 
                        count(1) 
                    from bimbingan bb 
                    where bb.id_mahasiswa = bm.id_mahasiswa 
                    and status = 1
                )
from bimbingan bm
inner join mahasiswa mh on bm.id_mahasiswa = mh.id
where (
                    select 
                        count(1) 
                    from bimbingan bb 
                    where bb.id_mahasiswa = bm.id_mahasiswa 
                    and status = 1
                ) >= 8$$

DROP EVENT `eventLulusBimbinganGenap`$$
CREATE DEFINER=`root`@`localhost` EVENT `eventLulusBimbinganGenap` ON SCHEDULE EVERY 1 YEAR STARTS '2021-03-17 12:00:00' ENDS '2021-03-17 12:30:00' ON COMPLETION NOT PRESERVE ENABLE DO insert into lulus_bimbingan (id_mahasiswa, semester, jumlah_bimbingan)
select 
	distinct
	mh.id,
	mh.semester,
	(
                    select 
                        count(1) 
                    from bimbingan bb 
                    where bb.id_mahasiswa = bm.id_mahasiswa 
                    and status = 1
                )
from bimbingan bm
inner join mahasiswa mh on bm.id_mahasiswa = mh.id
where (
                    select 
                        count(1) 
                    from bimbingan bb 
                    where bb.id_mahasiswa = bm.id_mahasiswa 
                    and status = 1
                ) >= 8$$

DROP EVENT `eventTidakLulusBimbinganGenap`$$
CREATE DEFINER=`root`@`localhost` EVENT `eventTidakLulusBimbinganGenap` ON SCHEDULE EVERY 1 YEAR STARTS '2021-03-17 12:00:00' ENDS '2021-03-17 12:30:00' ON COMPLETION NOT PRESERVE ENABLE DO insert into tidak_lulus_bimbingan (id_mahasiswa, semester, jumlah_bimbingan)
select 
	distinct
	mh.id,
	mh.nim,
	mh.nama,
	(
                    select 
                        count(1) 
                    from bimbingan bb 
                    where bb.id_mahasiswa = bm.id_mahasiswa 
                    and status = 1
                )
from bimbingan bm
inner join mahasiswa mh on bm.id_mahasiswa = mh.id
where (
                    select 
                        count(1) 
                    from bimbingan bb 
                    where bb.id_mahasiswa = bm.id_mahasiswa 
                    and status = 1
                ) < 8$$

DELIMITER ;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
