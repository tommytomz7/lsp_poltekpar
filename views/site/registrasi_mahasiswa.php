<?php
use backend\assets\AppAsset;
use yii\helpers\Html;

use yii\bootstrap\ActiveForm;
// use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\models\ProgramStudi;
use kartik\date\DatePicker;
use budyaga\cropper\Widget;
use yii\helpers\Url;
/* @var $this \yii\web\View */
/* @var $content string */

dmstr\web\AdminLteAsset::register($this);

$this->title = 'Registrasi Mahasiswa';

$fieldOptions1 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-envelope form-control-feedback'></span>"
];

$fieldOptions2 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-lock form-control-feedback'></span>"
];

$dataprodi=ArrayHelper::map(ProgramStudi::find()->all(),'id','nama_prodi');

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <link rel="shortcut icon" href="<?=Yii::$app->request->baseUrl?>/images/logo_akpar50.png">

</head>
<body class="login-page" style="background: url(<?=Yii::$app->request->baseUrl?>/images/akparmedan.jpg) no-repeat left fixed;">

<?php $this->beginBody() ?>
    
    <div  class="col-md-6">

    </div>
        <div class="col-md-6" style="margin-top: 10px; margin-bottom: 10px;">
    
    <!-- /.login-logo -->
    <div class="login-box-body">
        <div class="login-logo">
            <a href="#"><b style="font-size: 20pt;">POLITEKNIK PARIWISATA MEDAN</b></a>
            <center><div style="font-size: 15pt;">Registrasi Mahasiswa</div></center>
        </div>
       <!-- center><img src="<?=Yii::$app->request->baseUrl?>/images/logo_akpar50.png"></center><br> -->
       <!--  <p class="login-box-msg"><b>SISTEM INFORMASI KEUANGAN</b></p> -->

    <div class="mahasiswa-form">

        <?php $form = ActiveForm::begin(); ?>

        <?php if (Yii::$app->session->hasFlash('success')): ?>
            <div class="alert alert-success alert-dismissable">
                 <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                 <!-- <h4><i class="icon fa fa-check"></i>Saved!</h4> -->
                 <?= Yii::$app->session->getFlash('success') ?>
            </div>
        <?php endif; ?>


        <?php if (Yii::$app->session->hasFlash('error')): ?>
            <div class="alert alert-danger alert-dismissable">
                 <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                 <!-- <h4><i class="icon fa fa-check"></i>Saved!</h4> -->
                 <?= Yii::$app->session->getFlash('error') ?>
            </div>
        <?php endif; ?>

        <div class="row pull-center">
            <div class="col-md-12">
                
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">

                <?= $form->field($model, 'foto')->widget(Widget::className(), [
                    'uploadUrl' => Url::toRoute('user/uploadPhoto'),
                    'width'     => 512,
                    'height'    => 512,
                ]) ?>

            </div>
            
            <div class="col-md-6">

                <?= $form->field($model, 'idprodi')->widget(Select2::classname(), [
                    'data' => $dataprodi,
                    'options' => ['placeholder' => 'Pilih'],
                    'pluginOptions' => [
                        'allowClear' => false,
                    ],
                ]); ?>

                <?= $form->field($model, 'nim')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'kelas')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'semester')->widget(Select2::classname(), [
                    'data' => [
                        '1' => '1',
                        '2' => '2',
                        '3' => '3',
                        '4' => '4',
                        '5' => '5',
                        '6' => '6',
                        '7' => '7',
                        '8' => '8',
                    ],
                    'options' => ['placeholder' => 'Pilih'],
                    'pluginOptions' => [
                        'allowClear' => false,
                    ],
                ]); ?>

                <?= $form->field($model, 'tempat_lahir')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'tanggal_lahir')->widget(DatePicker::classname(), [
                    // 'options' => ['placeholder' => 'Enter birth date ...'],
                    'pluginOptions' => [
                        'format' => 'yyyy-mm-dd',
                        'autoclose' => true,
                        'todayHighlight' => true
                    ]
                ]); 

                ?>
                

                <?= $form->field($model, 'alamat')->textarea(['rows' => 6]) ?>
            </div>
        </div>
        
        <?php if (!Yii::$app->request->isAjax){ ?>
            <div class="form-group">
                <?= Html::submitButton('Registrasi', ['class' => 'btn btn-success']) ?>

                <?= Html::a('Login','?r=site/login', ['class'=>'btn btn-primary']) ?>
            </div>
        <?php } ?>

        <?php ActiveForm::end(); ?>
        
    </div>

    <!-- /.login-box-body -->
</div><!-- /.login-box -->
    </div>
    


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

