<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\UploadApl01 */
?>
<div class="upload-apl01-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
