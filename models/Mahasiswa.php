<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mahasiswa".
 *
 * @property int $id
 * @property string $nim
 * @property string $nama
 * @property string $jenis_kelamin
 * @property int $idprogramstudi
 * @property string $tanggal
 *
 * @property ProgramStudi $programstudi
 */
class Mahasiswa extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mahasiswa';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nim', 'nama', 'jenis_kelamin', 'idprogramstudi'], 'required'],
            [['idprogramstudi'], 'integer'],
            [['tanggal'], 'safe'],
            [['nim'], 'string', 'max' => 20],
            [['nama'], 'string', 'max' => 200],
            [['jenis_kelamin'], 'string', 'max' => 10],
            [['idprogramstudi'], 'exist', 'skipOnError' => true, 'targetClass' => ProgramStudi::className(), 'targetAttribute' => ['idprogramstudi' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nim' => 'Nim',
            'nama' => 'Nama',
            'jenis_kelamin' => 'Jenis Kelamin',
            'idprogramstudi' => 'Program Studi',
            'tanggal' => 'Tanggal',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProgramstudi()
    {
        return $this->hasOne(ProgramStudi::className(), ['id' => 'idprogramstudi']);
    }
}
