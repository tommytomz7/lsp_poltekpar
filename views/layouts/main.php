<?php
use yii\helpers\Html;

// if (class_exists('backend\assets\AppAsset')) {
//    backend\assets\AppAsset::register($this);
// } else {
//    app\assets\AppAsset::register($this);
// }

// dmstr\web\AdminLteAsset::register($this);

// $directoryAsset = Yii::$app->assetManager->getPublishedUrl('@vendor/almasaeed2010/adminlte/dist');
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
      <meta charset="<?= Yii::$app->charset ?>"/>
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <?= Html::csrfMetaTags() ?>
      <title>LSP POLTEKPAR MEDAN</title>
      <?php $this->head() ?>
        <!-- <link rel="shortcut icon" href="<?=Yii::$app->request->baseUrl?>/images/logo_akpar50.png"> -->
        <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> -->
        <!-- bootstrap css -->
      <link rel="stylesheet" href="<?=Yii::$app->request->baseUrl?>/covido/css/bootstrap.min.css">
      <!-- style css -->
      <link rel="stylesheet" href="<?=Yii::$app->request->baseUrl?>/covido/css/style.css">
      <!-- Responsive-->
      <link rel="stylesheet" href="<?=Yii::$app->request->baseUrl?>/covido/css/responsive.css">
      <!-- fevicon -->
      <link rel="icon" href="<?=Yii::$app->request->baseUrl?>/covido/images/fevicon.png" type="image/gif" />
      <!-- Scrollbar Custom CSS -->
      <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">
      <link rel="stylesheet" href="<?=Yii::$app->request->baseUrl?>/covido/css/owl.carousel.min.css"> 
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css" media="screen">
      <link rel="stylesheet" href="https://rawgit.com/LeshikJanz/libraries/master/Bootstrap/baguetteBox.min.css">
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
      <style type="text/css">
         .header-area {
            padding: 30px 16px;
            color: #f1f1f1;
            width: 100%;
            z-index: 9999999;
            height: 87px;
            background: #336;
         }
      </style>
    </head>

   <!-- body -->
   <?php
   if(isset($_GET['r'])){ ?>
      <body class="main-layout inner_page">
   <?php } else{ ?>
      <body class="main-layout">
   <?php } ?>
   <?php $this->beginBody() ?>
      <!-- loader  -->
      <div class="loader_bg">
         <div class="loader"><img src="<?=Yii::$app->request->baseUrl?>/covido/images/loading.gif" alt="#"/></div>
      </div>

        <?= $this->render(
            'header.php',
            []
            // ['directoryAsset' => $directoryAsset]
        ) ?>

        <?= $this->render(
            'content.php',
            ['content' => $content]
        ) ?>

        <?php $this->endBody() ?>

    <!-- Javascript files-->
   <script src="<?=Yii::$app->request->baseUrl?>/covido/js/jquery.min.js"></script>
   <script src="<?=Yii::$app->request->baseUrl?>/covido/js/bootstrap.bundle.min.js"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.js"></script>
   <script src="<?=Yii::$app->request->baseUrl?>/covido/js/owl.carousel.min.js"></script>
   <script src="<?=Yii::$app->request->baseUrl?>/covido/js/custom.js"></script> 
      
   </body>
</html>
   
<?php $this->endPage() ?>