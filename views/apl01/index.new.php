<?php
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Jadwal Pendaftaran';
$this->params['breadcrumbs'][] = $this->title;

?>
<!-- <link href='https://fonts.googleapis.com/css?family=Titillium+Web:400,200,300,600,700' rel='stylesheet' type='text/css'> -->
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
<style type="text/css">
    .timeline {
        list-style-type: none;
        display: flex;
        align-items: center;
        justify-content: center;
    }

    .li {
        transition: all 200ms ease-in;
    }

    .timestamp {
        margin-bottom: 20px;
        padding: 0px 40px;
        display: flex;
        flex-direction: column;
        align-items: center;
        font-weight: 100;
        text-align: center;
        font-size: 11px;
        font-we
    }

    .status {
        /* padding: 0px 40px; */
        padding: 0px 27px;
        display: flex;
        justify-content: center;
        border-top: 2px solid #D6DCE0;
        position: relative;
        transition: all 200ms ease-in;
    }

    .status h4 {
        font-weight: 600;
        font-size: 11px;
        padding-top: 16px;
        text-align: center;
    }

    .status:before {
        content: "";
        width: 25px;
        height: 25px;
        background-color: white;
        border-radius: 25px;
        border: 1px solid #ddd;
        position: absolute;
        top: -15px;
        left: 42%;
        transition: all 200ms ease-in;
    }

    .li.complete .status {
        border-top: 2px solid #66DC71;
    }

    .li.complete .status:before {
        background-color: #66DC71;
        border: none;
        transition: all 200ms ease-in;
    }

    .li.complete .status h4 {
        color: #66DC71;
    }

    @media (min-device-width: 320px) and (max-device-width: 700px) {
        .timeline {
            list-style-type: none;
            display: block;
        }

        .li {
            transition: all 200ms ease-in;
            display: flex;
            width: inherit;
        }

        .timestamp {
            width: 100px;
        }

        .status:before {
            left: -8%;
            top: 30%;
            transition: all 200ms ease-in;
        }
    }

    /* html, body {
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  font-family: "Titillium Web", sans serif;
  color: #758D96;
} */

    /* button {
  position: absolute;
  width: 100px;
  min-width: 100px;
  padding: 20px;
  margin: 20px;
  font-family: "Titillium Web", sans serif;
  border: none;
  color: white;
  font-size: 16px;
  text-align: center;
} */

    #toggleButton {
        position: absolute;
        left: 50px;
        top: 20px;
        background-color: #75C7F6;
    }
</style>

<?php
$step_registrasi = 1;
// $id_registrasi = Yii::$app->user->identity->id_registrasi;
// $id_user = ;
$step = [
    1 => "Kartu Keluarga",
    2 => "Bentuk Pendidikan & Jalur Pendaftaran",
    3 => "Pemilihan Sekolah & Jurusan",
    4 => "Koordinat Alamat Rumah",
    5 => "Selesai Registrasi"
];
?>

<center>
    <br><br>
    <h4><b>Progress Pendaftaran PPDB Tebing Tinggi Tahun Ajaran 2021/2022</b></h4>
</center><br><br><br><br><br>

<ul class="timeline" id="timeline">
    <?php
    for ($x = 1; $x <= count($step); $x++) {
        $status = '';

        $ket = '';
        if ($x <= $step_registrasi) {
            $status = 'complete';
            $ket =  "Selesai";
        } else {
            $ket =  "Belum Selesai";
        }
        ?>
        <li class='li <?= $status ?> '>
            <div class="timestamp">
                <span class="author"> <b><?= $step[$x] ?></b></span>
            </div>
            <div class="status">
                <h4> <?= $ket ?> </h4>
            </div>
        </li>

    <?php   } ?>

</ul>

<br>
<div class="container">
    <div class="row" style="margin-top: 100px;">
        <div class="col-md-12">
            <div class="text-center">
                <?= Html::a('Registrasi Online', ['site/registrasi-online'], ['class' => 'btn submit_btn']) ?>

                <?php
                // if ($step_registrasi == 5) {
                //      echo  Html::a('Cetak Bukti Pendaftaran', ['site/cetak', 'id'=> $id_registrasi], ['class' => 'btn submit_btn']);
               
                // }

                ?>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    // var completes = document.querySelectorAll(".complete");
    // var toggleButton = document.getElementById("toggleButton");


    // function toggleComplete() {
    //     var lastComplete = completes[completes.length - 1];
    //     lastComplete.classList.toggle('complete');
    // }

    toggleButton.onclick = toggleComplete;
</script>