<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "jenis_standar".
 *
 * @property int $id
 * @property string $nama
 *
 * @property UnitSertfikasi[] $unitSertfikasis
 */
class JenisStandar extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'jenis_standar';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnitSertfikasis()
    {
        return $this->hasMany(UnitSertfikasi::className(), ['idjenisstandar' => 'id']);
    }
}
