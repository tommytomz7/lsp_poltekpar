<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "upload_apl01".
 *
 * @property int $id
 * @property int $idapl01
 * @property string $portopolio
 * @property string $pas_photo
 * @property string $ktm_ktp
 * @property string $ijazah
 * @property string $sertifikat_pkl
 * @property string $tanggal
 *
 * @property Apl01 $apl01
 */
class UploadApl01 extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'upload_apl01';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idapl01', 'portopolio', 'pas_photo', 'ktm_ktp', 'ijazah'], 'required'],
            [['idapl01', 'portopolio', 'pas_photo', 'ktm_ktp', 'ijazah'], 'file'],
            [['idapl01'], 'integer'],
            [['tanggal'], 'safe'],
            // [['portopolio', 'pas_photo', 'ktm_ktp', 'ijazah', 'sertifikat_pkl'], 'string', 'max' => 100],
            [['idapl01'], 'exist', 'skipOnError' => true, 'targetClass' => Apl01::className(), 'targetAttribute' => ['idapl01' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idapl01' => 'Idapl01',
            'portopolio' => 'Portopolio',
            'pas_photo' => 'Pas Photo',
            'ktm_ktp' => 'KTM/KTP',
            'ijazah' => 'Ijazah',
            'sertifikat_pkl' => 'Sertifikat PKL(jika ada)',
            'tanggal' => 'Tanggal',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApl01()
    {
        return $this->hasOne(Apl01::className(), ['id' => 'idapl01']);
    }
}
