<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\models\TandaPengenal;

/* @var $this yii\web\View */
/* @var $model app\models\Apl01 */
/* @var $form yii\widgets\ActiveForm */
$datatandapengenal=ArrayHelper::map(TandaPengenal::find()->all(),'id','nama');
?>

<div class="apl01-form">

    <?php $form = ActiveForm::begin(); ?>

    
    
    <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'idtandapengenal')
        ->dropDownList(
            $datatandapengenal,           // Flat array ('id'=>'label')
            ['prompt'=>'Pilih']    // options
        );
    ?>

    <?= $form->field($model, 'tempat_lahir')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tanggal_lahir')->textInput() ?>

    <?= $form->field($model, 'jenis_kelamin')
        ->dropDownList(
            ['Laki-laki'=>'Laki-laki', 'Perempuan'=>'Perempuan'],           // Flat array ('id'=>'label')
            ['prompt'=>'Pilih']    // options
        );
    ?>
    
    <?= $form->field($model, 'kebangsaan')
        ->dropDownList(
            ['WNI'=>'WNI', 'WNA'=>'WNA'],           // Flat array ('id'=>'label')
            ['prompt'=>'Pilih']    // options
        );
    ?>

    <?= $form->field($model, 'alamat_rumah')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'kodepos')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'telp')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pendidikan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'perusahaan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'jabatan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'alamat_kantor')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'kodepos_kantor')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'telp_kantor')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email_kantor')->textInput(['maxlength' => true]) ?>
	

    <?php ActiveForm::end(); ?>
    
</div>
