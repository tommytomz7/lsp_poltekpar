<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "unit_sertfikasi".
 *
 * @property int $id
 * @property int $idskemasertifikasi
 * @property string $kode_unit
 * @property string $judul_unit
 * @property int $idjenisstandar
 *
 * @property Apl01[] $apl01s
 * @property JenisStandar $jenisstandar
 * @property SkemaSertifikasi $skemasertifikasi
 */
class UnitSertfikasi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'unit_sertfikasi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idskemasertifikasi', 'kode_unit', 'judul_unit', 'idjenisstandar'], 'required'],
            [['idskemasertifikasi', 'idjenisstandar'], 'integer'],
            [['kode_unit'], 'string', 'max' => 100],
            [['judul_unit'], 'string', 'max' => 200],
            [['idjenisstandar'], 'exist', 'skipOnError' => true, 'targetClass' => JenisStandar::className(), 'targetAttribute' => ['idjenisstandar' => 'id']],
            [['idskemasertifikasi'], 'exist', 'skipOnError' => true, 'targetClass' => SkemaSertifikasi::className(), 'targetAttribute' => ['idskemasertifikasi' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idskemasertifikasi' => 'Skema Sertifikasi',
            'kode_unit' => 'Kode Unit',
            'judul_unit' => 'Judul Unit',
            'idjenisstandar' => 'Jenis Standar',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApl01s()
    {
        return $this->hasMany(Apl01::className(), ['idunitsertifikasi' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJenisstandar()
    {
        return $this->hasOne(JenisStandar::className(), ['id' => 'idjenisstandar']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSkemasertifikasi()
    {
        return $this->hasOne(SkemaSertifikasi::className(), ['id' => 'idskemasertifikasi']);
    }
}
