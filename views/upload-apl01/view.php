<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\UploadApl01 */
?>
<div class="upload-apl01-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'idapl01',
            'portopolio',
            'pas_photo',
            'ktm_ktp',
            'ijazah',
            'sertifikat_pkl',
            'tanggal',
        ],
    ]) ?>

</div>
