<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\UnitSertfikasi */
?>
<div class="unit-sertfikasi-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
