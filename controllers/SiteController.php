<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\User;
use app\models\Mahasiswa;
use app\models\Dosen;
use app\models\AdminLayanan;
use app\models\Percakapan;
use app\models\searchs\PercakapanSearch;
use app\models\searchs\MahasiswaSearch;
use app\models\searchs\BimbinganSearch;
use mdm\admin\models\form\ChangePassword;
use app\models\PercakapanLayananDosen;
use app\models\searchs\PercakapanLayananDosenSearch;
use yii\web\UploadedFile;
use app\models\PercakapanLayananProdi;
use app\models\searchs\PercakapanLayananProdiSearch;
use app\models\AdminProdi;
use app\models\AdminMahasiswa;
use mdm\admin\components\UserStatus;
use yii\helpers\ArrayHelper;
use mdm\admin\models\Assignment;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {   
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {   
        // $model = new LoginForm();
        // Yii::$app->user->logout();
        Yii::$app->session->destroy();
        //return $this->render('login', ['model'=>$model]);
        return $this->redirect('?r=site/login');

        
    }

    public function actionRegistrasiMahasiswa(){
        $this->layout = false;
        $transaction = Yii::$app->db->beginTransaction();
        $request = Yii::$app->request;
        $model = new Mahasiswa();
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        if ($model->load($request->post()) && $model->save()) {
            $modeluser = new User();
            $modeluser->id_user     = $model->id;
            $modeluser->role        = "Mahasiswa";
            $modeluser->status      = ArrayHelper::getValue(Yii::$app->params, 'user.defaultStatus', UserStatus::ACTIVE);
            $modeluser->username    = $model->nim;
            $modeluser->setPassword($model->nim);
            $modeluser->generateAuthKey();
            if($modeluser->save()){
                $modelassign = new Assignment($modeluser->id);
                $modelassign->assign(['Mahasiswa']);
                $transaction->commit();
                Yii::$app->session->setFlash('success', "Registrasi Berhasil");
                return $this->redirect('?r=site/registrasi-mahasiswa');
            }else{
                $transaction->rollBack();
                Yii::$app->session->setFlash('error', "Registrasi Gagal");
                return $this->redirect('?r=site/registrasi-mahasiswa');
            }
            // print_r($model->getErrors());
            // die();
            
            // return $this->render('registrasi_mahasiswa', [
            //     'model' => $model,
            // ]);
            
        }
        return $this->render('registrasi_mahasiswa', [
            'model' => $model,
        ]);
        

        // return $this->render('registrasi_mahasiswa', [
        //     'model' => $model,
        // ]);
        // return $this->redirect('?r=site/registrasi_mahasiswa');
       
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionKirimemail(){

        $vpass = "sqs123456";
        $model = User::findOne(['email' => $_GET['email']]);
        $model->generateAuthKey();
        $model->setPassword($vpass);
        $model->save();

        //$dataemail = User::findOne(['email' => $_GET['email']]);

        $kirim = Yii::$app->mailer->compose()
        ->setFrom('support_tapem@pematangsiantarkota.com')
        ->setTo($_GET['email'])
        ->setSubject('Reset Password')
        ->setTextBody('Plain text content')
        ->setHtmlBody('<b>Password Baru: '.$vpass.'</b>')
        ->send();

        if($kirim){
            $pesan = array('pesan'=>'Password berhasil direset, Silahkan cek email Anda');
        }else{
            $pesan = array('pesan'=>'Proses gagal');
        }
        //$pesan = array('pesan'=>$dataemail->password_hash);
        echo json_encode($pesan);
    }

    public function actionGantiPassword(){
        $model = new ChangePassword();
        if ($model->load(Yii::$app->getRequest()->post()) && $model->change()) {
            return $this->goHome();
        }

        return $this->render('ganti-password', [
                'model' => $model,
        ]);

    }
}
