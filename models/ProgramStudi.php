<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "program_studi".
 *
 * @property int $id
 * @property string $nama_prodi
 *
 * @property Mahasiswa[] $mahasiswas
 */
class ProgramStudi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'program_studi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama_prodi'], 'required'],
            [['nama_prodi'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_prodi' => 'Nama Prodi',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMahasiswas()
    {
        return $this->hasMany(Mahasiswa::className(), ['idprogramstudi' => 'id']);
    }
}
