<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SkemaSertifikasi */
?>
<div class="skema-sertifikasi-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
