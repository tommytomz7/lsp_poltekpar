<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\TandaPengenal */
?>
<div class="tanda-pengenal-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'nama',
        ],
    ]) ?>

</div>
