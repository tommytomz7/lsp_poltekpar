<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TandaPengenal */

?>
<div class="tanda-pengenal-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
