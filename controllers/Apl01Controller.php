<?php

namespace app\controllers;

use Yii;
use app\models\Apl01;
use app\models\searchs\Apl01Search;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use app\models\searchs\UnitSertfikasiSearch;
use app\models\UploadApl01;
use yii\web\UploadedFile;

/**
 * Apl01Controller implements the CRUD actions for Apl01 model.
 */
class Apl01Controller extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Apl01 models.
     * @return mixed
     */
    public function actionIndex()
    {    
        // $searchModel = new Apl01Search();
        // $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $request = Yii::$app->request;
        $transaction = Yii::$app->db->beginTransaction();

        $step = 0;
        // if (isset(Yii::$app->session->id)) {
            $model = new Apl01(); 
            $modeluploadapl01 = new UploadApl01(); 

            if(isset($_GET['step_registrasi'])){
                $model = new Apl01(); 
                $modeluploadapl01 = new UploadApl01(); 
                $step = 1;
                $model->idunitsertifikasi = $_GET['id'];
            }

            if($model->load($request->post())){

                // if($modeluploadapl01->load($request->post())){
                    $modeluploadapl01->portopolio = UploadedFile::getInstance($modeluploadapl01, 'portopolio');
                    $modeluploadapl01->pas_photo = UploadedFile::getInstance($modeluploadapl01, 'pas_photo');
                    $modeluploadapl01->ktm_ktp = UploadedFile::getInstance($modeluploadapl01, 'ktm_ktp');
                    $modeluploadapl01->ijazah = UploadedFile::getInstance($modeluploadapl01, 'ijazah');
                    $modeluploadapl01->sertifikat_pkl = UploadedFile::getInstance($modeluploadapl01, 'sertifikat_pkl');

                    $modeluploadapl01->portopolio->saveAs('uploads/' . $modeluploadapl01->portopolio->baseName . '.' . $modeluploadapl01->portopolio->extension);
                    $modeluploadapl01->pas_photo->saveAs('uploads/' . $modeluploadapl01->pas_photo->baseName . '.' . $modeluploadapl01->pas_photo->extension);
                    $modeluploadapl01->ktm_ktp->saveAs('uploads/' . $modeluploadapl01->ktm_ktp->baseName . '.' . $modeluploadapl01->ktm_ktp->extension);
                    $modeluploadapl01->ijazah->saveAs('uploads/' . $modeluploadapl01->ijazah->baseName . '.' . $modeluploadapl01->ijazah->extension);
                    $modeluploadapl01->sertifikat_pkl->saveAs('uploads/' . $modeluploadapl01->sertifikat_pkl->baseName . '.' . $modeluploadapl01->sertifikat_pkl->extension);

                    if($model->save()){
                        $modeluploadapl01->idapl01 = $model->id;
                        if($modeluploadapl01->save()){
                            $step = 2;
                            Yii::$app->session->setFlash('success', "Registrasi Berhasil"); 
                            $transaction->commit();
                            return $this->render('index', [
                                'step' => $step,
                            ]);
                        }else{
                            $step = 2;
                            $searchModel = new UnitSertfikasiSearch();
                            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
                
                            return $this->render('index', [
                                'searchModel' => $searchModel,
                                'dataProvider' => $dataProvider,
                                'model' => $model,
                                'modeluploadapl01' => $modeluploadapl01,
                                'step' => $step,
                            ]);
                        }
                        
                    }else{
                        $step = 2;
                        $searchModel = new UnitSertfikasiSearch();
                        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            
                        return $this->render('index', [
                            'searchModel' => $searchModel,
                            'dataProvider' => $dataProvider,
                            'model' => $model,
                            'modeluploadapl01' => $modeluploadapl01,
                            'step' => $step,
                        ]);
                    }
            }else{
                $searchModel = new UnitSertfikasiSearch();
                $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
    
                return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'model' => $model,
                    'modeluploadapl01' => $modeluploadapl01,
                    'step' => $step,
                ]);
            }
           
        // }
        // else{
        //     return $this->redirect(['site/login']);
        // }
        
    }


    /**
     * Displays a single Apl01 model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "Detail Apl01",
                    'content'=>$this->renderAjax('view', [
                        'model' => $this->findModel($id),
                    ]),
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Ubah',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new Apl01 model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Apl01();  

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Tambah Apl01",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Tambah Apl01",
                    'content'=>'<span class="text-success">Tambah Apl01 Sukses</span>',
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Tambah Lagi',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])
        
                ];         
            }else{           
                return [
                    'title'=> "Tambah Apl01",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
       
    }

    /**
     * Updates an existing Apl01 model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Ubah Apl01",
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Apl01",
                    'content'=>$this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Ubah',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
            }else{
                 return [
                    'title'=> "Ubah Apl01",
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing Apl01 model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

     /**
     * Delete multiple existing Apl01 model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
       
    }

    /**
     * Finds the Apl01 model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Apl01 the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Apl01::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
