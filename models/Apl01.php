<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "apl01".
 *
 * @property int $id
 * @property int $idunitsertifikasi
 * @property string $nama
 * @property int $idtandapengenal
 * @property string $tempat_lahir
 * @property string $tanggal_lahir
 * @property string $jenis_kelamin
 * @property string $kebangsaan
 * @property string $alamat_rumah
 * @property string $kodepos
 * @property string $telp
 * @property string $email
 * @property string $pendidikan
 * @property string $perusahaan
 * @property string $jabatan
 * @property string $alamat_kantor
 * @property string $kodepos_kantor
 * @property string $telp_kantor
 * @property string $email_kantor
 * @property int $status 1 = setuju, 0=belum disetujui
 * @property string $tanggal
 *
 * @property UnitSertfikasi $unitsertifikasi
 * @property UploadApl01[] $uploadApl01s
 */
class Apl01 extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'apl01';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idunitsertifikasi', 'nama', 'idtandapengenal', 'tempat_lahir', 'tanggal_lahir', 'jenis_kelamin', 'kebangsaan', 'alamat_rumah', 'telp', 'pendidikan', 'perusahaan', 'jabatan', 'alamat_kantor'], 'required'],
            [['idunitsertifikasi', 'idtandapengenal', 'status'], 'integer'],
            [['tanggal_lahir', 'tanggal'], 'safe'],
            [['alamat_rumah', 'alamat_kantor'], 'string'],
            [['nama'], 'string', 'max' => 200],
            [['tempat_lahir', 'email', 'perusahaan', 'email_kantor'], 'string', 'max' => 100],
            [['jenis_kelamin', 'kebangsaan', 'kodepos', 'kodepos_kantor'], 'string', 'max' => 10],
            [['telp', 'telp_kantor'], 'string', 'max' => 15],
            [['pendidikan'], 'string', 'max' => 50],
            [['jabatan'], 'string', 'max' => 20],
            [['idunitsertifikasi'], 'exist', 'skipOnError' => true, 'targetClass' => UnitSertfikasi::className(), 'targetAttribute' => ['idunitsertifikasi' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idunitsertifikasi' => 'Idunitsertifikasi',
            'nama' => 'Nama',
            'idtandapengenal' => 'Tanda Pengenal',
            'tempat_lahir' => 'Tempat Lahir',
            'tanggal_lahir' => 'Tanggal Lahir',
            'jenis_kelamin' => 'Jenis Kelamin',
            'kebangsaan' => 'Kebangsaan',
            'alamat_rumah' => 'Alamat Rumah',
            'kodepos' => 'Kodepos',
            'telp' => 'Telp',
            'email' => 'Email',
            'pendidikan' => 'Pendidikan',
            'perusahaan' => 'Perusahaan',
            'jabatan' => 'Jabatan',
            'alamat_kantor' => 'Alamat Kantor',
            'kodepos_kantor' => 'Kodepos Kantor',
            'telp_kantor' => 'Telp Kantor',
            'email_kantor' => 'Email Kantor',
            'status' => 'Status',
            'tanggal' => 'Tanggal',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnitsertifikasi()
    {
        return $this->hasOne(UnitSertfikasi::className(), ['id' => 'idunitsertifikasi']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUploadApl01s()
    {
        return $this->hasMany(UploadApl01::className(), ['idapl01' => 'id']);
    }
}
