<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ProgramStudi */

?>
<div class="program-studi-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
