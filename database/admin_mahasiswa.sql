/*
Navicat MySQL Data Transfer

Source Server         : koneksibitnami
Source Server Version : 80015
Source Host           : localhost:3306
Source Database       : konseling

Target Server Type    : MYSQL
Target Server Version : 80015
File Encoding         : 65001

Date: 2021-03-01 15:00:03
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for admin_mahasiswa
-- ----------------------------
DROP TABLE IF EXISTS `admin_mahasiswa`;
CREATE TABLE `admin_mahasiswa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nip` varchar(25) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `idjabatan` int(11) NOT NULL,
  `foto` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_admin_prodi_idjabatan` (`idjabatan`),
  CONSTRAINT `admin_mahasiswa_ibfk_1` FOREIGN KEY (`idjabatan`) REFERENCES `jabatan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of admin_mahasiswa
-- ----------------------------
INSERT INTO `admin_mahasiswa` VALUES ('4', '65444444', 'Putri', '2', '../media/foto/6038c66abff57.png');
INSERT INTO `admin_mahasiswa` VALUES ('5', '999900001', 'Admin Sukaati', '1', null);
INSERT INTO `admin_mahasiswa` VALUES ('6', '787666', 'tess', '2', null);
SET FOREIGN_KEY_CHECKS=1;
