<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\UnitSertfikasi */
?>
<div class="unit-sertfikasi-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'idskemasertifikasi',
            'kode_unit',
            'judul_unit',
            'idjenisstandar',
        ],
    ]) ?>

</div>
