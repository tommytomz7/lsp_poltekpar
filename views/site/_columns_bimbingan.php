<?php
use yii\helpers\Url;
use yii\helpers\Html;

// $group = array();
// $nim = array();
// $aksi = array();

// if(Yii::$app->user->identity->role=='Mahasiswa'){
//    $group = [
//         'class'=>'\kartik\grid\DataColumn',
//         'attribute'=>'id_dosen',
//         'value'=>'dosen.nama',
//     ];

//     $aksi = [
//         'class' => 'kartik\grid\ActionColumn',
//         'dropdown' => false,
//         'vAlign'=>'middle',
//         'template' => "{view}",
//         'buttons'=>[
//         'view' => function ($url, $model) {
//                 return Html::a('<span class="glyphicon glyphicon-eye-open btn btn-info btn-xs rounded"></span>', $url, [
//                             'title' => Yii::t('app', 'Detail Bimbingan'),
//                             'role'=>'modal-remote',
//                             'data-toggle'=>'tooltip'
//                 ]);
//             },
//         ]
//         // 'urlCreator' => function($action, $model, $key, $index) { 
//         //         return Url::to([$action,'id'=>$key]);
//         // },
//         // 'viewOptions'=>['role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip'],
//     ];
// }else if(Yii::$app->user->identity->role=='Dosen'){

// }else{
//     $group = [
//         [
//             'class'=>'\kartik\grid\DataColumn',
//             'attribute'=>'id_mahasiswa',
//         ],
//         [
//             'class'=>'\kartik\grid\DataColumn',
//             'attribute'=>'id_dosen',
//         ],
//     ];
// }

if(Yii::$app->user->identity->role=='Mahasiswa'){
    return [
        // [
        //     'class' => 'kartik\grid\CheckboxColumn',
        //     'width' => '20px',
        // ],
        [
            'class' => 'kartik\grid\SerialColumn',
            'width' => '30px',
        ],
            // [
            // 'class'=>'\kartik\grid\DataColumn',
            // 'attribute'=>'id',
        // ],
        [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'id_mahasiswa',
            'label' => 'Nim',
            'value'=>'mahasiswa.nim',
        ],
        [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'id_mahasiswa',
            'label' => 'Nama Mahasiswa',
            'value'=>'mahasiswa.nama',
        ],
        [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'file',
            'value'=>function($data){
                return Html::a($data->file,'../media/file_bimbingan/'.$data->file, ['target'=>'_blank', 'data-pjax'=>'0']);
            },
            'visible' => true,
            'format' => 'raw'
        ],
        [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'komentar',
            'value' => function($data){
                if(!empty($data->komentar)){
                    return $data->komentar;
                }else{
                    return "";
                }
            }
        ],
        [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'status',
            'format'    => 'raw',
            'value' => function($data){
                if($data->status=="0"){
                    return "<font color=red>Ditolak</font>";
                }else if($data->status=="1"){
                    return "<font color=green>Disetujui</font>";
                }else if($data->status==""){
                    return "<font color=grey>Menunggu</font>";
                }
            }
        ],
        [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'tanggal',
            'format' => ['date', 'php:d/m/Y H:i:s'],
        ],
        [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'template' => "{view}",
        'buttons'=>[
            'view' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-eye-open btn btn-info btn-xs rounded"></span>', $url, [
                                'title' => Yii::t('app', 'Detail Bimbingan'),
                                'role'=>'modal-remote',
                                'data-toggle'=>'tooltip'
                    ]);
                },
            ]
        // 'urlCreator' => function($action, $model, $key, $index) { 
        //         return Url::to([$action,'id'=>$key]);
        // },
        // 'viewOptions'=>['role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip'],
        ],
    ];   
}

if(Yii::$app->user->identity->role=='Dosen'){
    return [
        // [
        //     'class' => 'kartik\grid\CheckboxColumn',
        //     'width' => '20px',
        // ],
        [
            'class' => 'kartik\grid\SerialColumn',
            'width' => '30px',
        ],
            // [
            // 'class'=>'\kartik\grid\DataColumn',
            // 'attribute'=>'id',
        // ],
        [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'id_mahasiswa',
            'label' => 'Nim',
            'value'=>'mahasiswa.nim',
        ],
        [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'id_mahasiswa',
            'label' => 'Nama Mahasiswa',
            'value'=>'mahasiswa.nama',
        ],
        // [
        //     'class'=>'\kartik\grid\DataColumn',
        //     'attribute'=>'file',
        //     'value'=>function($data){
        //         return Html::a($data->file,'../media/file_bimbingan/'.$data->file, ['target'=>'_blank', 'data-pjax'=>'0']);
        //     },
        //     'visible' => true,
        //     'format' => 'raw'
        // ],
        // [
        //     'class'=>'\kartik\grid\DataColumn',
        //     'attribute'=>'komentar',
        //     'value' => function($data){
        //         if(!empty($data->komentar)){
        //             return $data->komentar;
        //         }else{
        //             return "";
        //         }
        //     }
        // ],
        [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'status',
            'format'    => 'raw',
            'value' => function($data){
                if($data->status=="0"){
                    return "<font color=red>Ditolak</font>";
                }else if($data->status=="1"){
                    return "<font color=green>Disetujui</font>";
                }else if($data->status==""){
                    return "<font color=grey>Menunggu</font>";
                }
            }
        ],
        // [
        //     'class'=>'\kartik\grid\DataColumn',
        //     'attribute'=>'tanggal',
        //     'format' => ['date', 'php:d/m/Y H:i:s'],
        // ],
        [
            'class' => 'kartik\grid\ActionColumn',
            'dropdown' => false,
            'vAlign'=>'middle',
            'template' => "{view}",
            'buttons'=>[
            'view' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-list btn btn-info btn-xs rounded"></span>', '?r=bimbingan/data-mahasiswa&Bimbingan[id_mahasiswa]='.$model->mahasiswa->id, [
                                'title' => Yii::t('app', 'Detail '.$model->mahasiswa->nama),
                                // 'role'=>'modal-remote',
                                // 'data-toggle'=>'tooltip'
                    ]);
                },
            ]
        ],
    ];   
}
