<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\models\ProgramStudi;

/* @var $this yii\web\View */
/* @var $model app\models\Mahasiswa */
/* @var $form yii\widgets\ActiveForm */

$dataprodi=ArrayHelper::map(ProgramStudi::find()->all(),'id','nama_prodi');

?>

<div class="about">
    <div class="container_width">
        <div class="titlepage text_align_left">
            <h2>Registrasi</h2>
            <p></p>
        </div>

        <div>
            <?php if (Yii::$app->session->hasFlash('success')): ?>
                <div class="alert alert-success alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <h4><i class="icon fa fa-check"></i>Saved!</h4>
                    <?= Yii::$app->session->getFlash('success') ?>
                </div>
            <?php endif; ?>

            <?php if (Yii::$app->session->hasFlash('error')): ?>
                <div class="alert alert-danger alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <h4><i class="icon fa fa-check"></i>Saved!</h4>
                    <?= Yii::$app->session->getFlash('error') ?>
                </div>
            <?php endif; ?>

            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'nim')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>
            
            <?= $form->field($model, 'jenis_kelamin')
                ->dropDownList(
                    ['Laki-laki' => 'Laki-laki', 'Perempuan'=>'Perempuan'],           // Flat array ('id'=>'label')
                    ['prompt'=>'Pilih']    // options
                );
            ?>

            <?= $form->field($model, 'idprogramstudi')
                ->dropDownList(
                    $dataprodi,           // Flat array ('id'=>'label')
                    ['prompt'=>'Pilih']    // options
                );
            ?>

            <?php if (!Yii::$app->request->isAjax){ ?>
                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? 'Simpan' : 'Ubah', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
            <?php } ?>

            <?php ActiveForm::end(); ?>
        </div>
        
    </div>
</div>

